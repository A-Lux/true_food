<?php

use App\GiftCard;
use Illuminate\Database\Seeder;

class GiftCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'thumbnail' => '',
                'price' => 2000
            ],
            [
                'thumbnail' => '',
                'price' => 5000
            ],
            [
                'thumbnail' => '',
                'price' => 10000
            ],
        ];

        GiftCard::insert($data);
    }
}
