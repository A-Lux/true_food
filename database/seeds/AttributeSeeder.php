<?php

use App\Attribute;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Вес',
                'slug' => 'ves',
                'attribute_type_id' => 1,
                'dimention_type_id' => 1
            ],
            [
                'name' => 'Срок годности',
                'slug' => 'srok-godnosti',
                'attribute_type_id' => 1,
                'dimention_type_id' => 2
            ]
        ];

        Attribute::insert($data);
    }
}
