<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call([
            // DimentionTypeSeeder::class,
            // DimentionSeeder::class,
            // AttributeTypeSeeder::class,
            // AttributeSeeder::class
            // GiftCardSeeder::class
            OrderTypeSeeder::class,
            StatusSeeder::class,
            PaymentTypeSeeder::class
        ]);
    }
}
