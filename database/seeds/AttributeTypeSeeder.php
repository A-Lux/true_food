<?php

use App\AttributeType;
use Illuminate\Database\Seeder;

class AttributeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'name' => 'checkbox'
            ],
            [
                'name' => 'integer_field'
            ]
        ];

        AttributeType::insert($types);
    }
}
