<?php

use App\OrderType;
use Illuminate\Database\Seeder;

class OrderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Блюдо',
                'is_dish' => 1
            ],
            [
                'name' => 'Подарочная карта',
                'is_dish' => 0
            ]
        ];

        OrderType::insert($data);
    }
}
