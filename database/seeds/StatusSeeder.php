<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Новый заказ',
                'status_number' => 1,
            ],
            [
                'name' => 'Готовиться',
                'status_number' => 2
            ],
            [
                'name' => 'Приготовлено',
                'status_number' => 3,
            ],
            [
                'name' => 'В пути',
                'status_number' => 4
            ],
            [
                'name' => 'Закрыт',
                'status_number' => 5
            ]
        ];
        
        Status::insert($data);
    }
}
