<?php

use App\DimentionType;
use Illuminate\Database\Seeder;

class DimentionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'name' => 'Вес'
            ],
            [
                'name' => 'Время'
            ]
        ];
        DimentionType::insert($types);
    }
}
