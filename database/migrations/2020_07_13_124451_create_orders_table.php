<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained()->onDelete('set null');
            $table->foreignId('order_type_id')->constrained();
            $table->foreignId('status_id')->constrained();
            $table->foreignId('payment_type_id')->constrained();
            $table->foreignId('delivery_type_id')->nullable()->constrained();
            $table->foreignId('delivery_place_id')->nullable()->constrained();
            $table->unsignedInteger('cabinet_number')->nullable();
            $table->timestamp('pick_up_at')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
