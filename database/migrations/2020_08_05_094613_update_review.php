<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateReview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('feedback', function(Blueprint $table){
            $table->foreignId('product_id')->after('user_id')->constrained();
            $table->float('rating');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('feedback',function(Blueprint $table){
           $table->dropColumn('product_id');
           $table->dropColumn('rating');
        });
    }
}
