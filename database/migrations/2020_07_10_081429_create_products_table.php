<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description');
            $table->string('thumbnail');
            $table->text('slider_images')->nullable();
            $table->boolean('is_popular')->default(0);
            $table->boolean('is_dish_of_the_week')->default(0);
            $table->boolean('is_new')->default(0);
            $table->foreignId('category_id')->constrained()->onDelete('set null');
            $table->timestamps();

            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *ц
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
