<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->string('middlename')->nullable()->after('name');
            $table->string('lastname')->nullable()->after('middlename');
            $table->string('phone')->nullable()->after('lastname');
            $table->string('birthday')->nullable()->after('phone');
            $table->foreignId('location_id')->nullable()->after('birthday')->constrained()->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('middlename');
            $table->dropColumn('lastname');
            $table->dropColumn('phone');
            $table->dropColumn('birthday');
            $table->dropForeign('location_id');
            $table->dropColumn('location_id');
        });
    }
}
