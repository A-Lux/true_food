<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'LoginController@login');
Route::post('register', 'RegisterController@register');
Route::get('categories/paginated', 'CategoryController@categoriesPaginated');

Route::prefix('places')
    ->group(function () {
        Route::get('/', 'PlaceController@show');
    });
Route::post('request', 'RequestController@store');
Route::get('order_type', 'OrderTypeController@show');
Route::get('payment_type', 'PaymentTypeController@show');
Route::get('pickup', 'PickupController@show');
//Route::prefix('password')
//    ->group(function () {
//        Route::post('create', 'PasswordResetController@create');
//        Route::get('find/{token}', 'PasswordResetController@find');
//        Route::post('reset', 'PasswordResetController@reset');
//    });

Route::get('about-us','AboutusController@index');
Route::middleware('auth:api')
    ->group(function () {
        Route::prefix('chat')
            ->group(function () {
                Route::get('', 'ChatController@show');
                Route::post('', 'ChatController@store');
                Route::get('all', 'ChatController@allMessages');
            });

        Route::post('logout', 'LogoutController@logout');
        Route::post('feedback', 'FeedbackController@send');
        Route::prefix('user')
            ->group(function () {
                Route::get('/', 'UserController@user');
                Route::put('/', 'UserController@update');
                Route::get('orders', 'UserController@orders');
                Route::get('orders/{order}', 'UserController@order');
                Route::get('add-card','CardController@addCard');
                Route::get('cards','CardController@getCards');
                Route::post('delete-card/{id}','CardController@deleteCard');
            });
    });
Route::prefix('review')->group(function () {
    Route::get('show', 'ReviewController@show');
    Route::post('store', 'ReviewController@store');
});
Route::get('categories', 'CategoryController@getCategories');

Route::prefix('orders')
    ->group(function () {
        Route::post('pickup', 'OrderController@pickup');
        Route::post('delivery', 'OrderController@delivery');
        Route::post('gift-card', 'OrderController@giftCard');
        Route::any('order/status', 'OrderController@orderStatus')->name('orderStatus');
    });

Route::get('toggle/product/{id}','ProductController@editShow');

Route::any('gift/result', 'OrderController@giftCardResult')->name('giftCardResult');
Route::prefix('products')
    ->group(function () {
        Route::get('/', 'ProductFilterController@filter');
        Route::get('{product}', 'ProductController@show');
        Route::get('{product}/variations', 'ProductController@variations');
    });
Route::get('/orders/count','OrderController@getOrderCount');
Route::get('/chats/count','ChatController@getCount');
Route::get('order/time-table','OrderController@timeTable');

Route::prefix('basket')
    ->group(function () {
        Route::get('show', 'BasketController@show');
        Route::get('mobile', 'BasketController@mobile');
        Route::post('add', 'BasketController@add');
        Route::post('increase','BasketController@increase');
        Route::post('delete', 'BasketController@delete');
        Route::post('delete/all', 'BasketController@deleteAll');
        Route::get('clear', 'BasketController@clear');
    });


Route::post('register-card','CardController@cardResult');
Route::get('filters', 'FilterController@getFilters');
Route::get('attributes', 'AttributeController@getAttributes');
Route::get('gift-cards', 'GiftCardController@show');
Route::get('contacts-info','AboutusController@contacts');
Route::post('forget-password','UserController@forgetPassword');
Route::get('socials','UserController@socials');
Route::post('link-card','CardController@saveCard')->name('saveCard');
