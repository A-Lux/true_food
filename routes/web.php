<?php

use App\Events\MessageSent;
use App\Product;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Front\IndexController@Index')->name('login');
Route::get('/auth', 'Front\IndexController@Auth')->name('auth_required');

Route::get('switch/{lang}', 'Front\IndexController@switch')->name('switch');
Route::get('/catalog/{slug?}', 'Front\CatalogController@Index')->name('Catalog');

Route::get('broadcast', 'Front\CatalogController@test');
Route::get('/products/{slug}', 'Front\ProductController@Show')->name('Product');
Route::get('success/order', 'Front\ProductController@Success')->name('orderSuccess');
//Route::get('categories/{slug}','Front\CatalogController@cateogry')->name('Category');
Route::get('fail/order', 'Front\ProductController@Fail')->name('orderFail');

Route::get('/cart', function () {
    $local = session()->get('lang');
    App::setLocale($local);
    $data['popularProducts'] = Product::where('is_popular', 1)->where('is_show',1)->get();
    return view('cart.index', $data);
});

Route::get('/cabinet/{any?}', function () {
    $local = session()->get('lang');
    App::setLocale($local);
    return view('cabinet.index');
})->where('any', '.*')->name('Personal');

Route::get('/giftcards', function () {
    $local = session()->get('lang');
    App::setLocale($local);
    $data['popularProducts'] = Product::where('is_popular', 1)->get();
    return view('giftcard.index', $data);
});

Route::get('/about', function () {
    $local = session()->get('lang');
    App::setLocale($local);
    return view('company.about');
});

Route::get('/terms', function () {
    return view('terms.index');
});

Route::get('/contacts', function () {
    $local = session()->get('lang');
    App::setLocale($local);
    return view('company.contacts');
});
Route::get('/payments', 'Front\ProductController@RequestPayment')->name('RequestPayment');

Route::group(['prefix' => 'admin', 'middleware' => ['make.slug']], function () {
    Voyager::routes();


    Route::get('chat', 'ChatController@index');
    Route::post('update/order/status/','Voyager\OrderController@updateStatus')->name('updateStatus');
    Route::post('add/message', 'ChatController@store')->name('storeChat');
    Route::get('chat/{id}', 'ChatController@show')->name('showChat');
    Route::get('order', 'Voyager\OrderController@orders')->name('orders');
    Route::get('prove/order/{id}', 'Voyager\OrderController@prove')->name('proveOrder');
    Route::get('order/{id}', 'Voyager\OrderController@show')->name('show');
    Route::post('products/{product}/variations', 'Voyager\VariationController@store');
    Route::post('products/{product}/variations/{productVariation}', 'Voyager\VariationController@update');
    Route::delete('variations/{productVariation}', 'Voyager\VariationController@destroy');
    Route::post('variations/{productVariation}/attributes', 'Voyager\AttributeController@store');
    Route::post('variations/{productVariation}/attributes/{productVariationAttribute}', 'Voyager\AttributeController@update');
    Route::delete('attributes/{productVariationAttribute}', 'Voyager\AttributeController@destroy');
    Route::get('notifications','NotificationController@show');
    Route::post('notifications','NotificationController@store');
});


Route::get('test/{message}', function ($message) {
    $query = [
        'pg_merchant_id' => 532183,
        'pg_amount' => 5000,
        'pg_salt' => Str::random(15),
        'pg_order_id' => 2,
        'pg_description' => 'hi',
        'pg_result_url' => route('orderStatus'),
        'pg_success_url' => route('orderSuccess'),
        'pg_success_url_method' => 'AUTOGET',

    ];

    $query['pg_testing_mode'] = 1; //add this parameter to request for testing payments

    //if you pass any of your parameters, which you want to get back after the payment, then add them. For example:
    // $request['client_name'] = 'My Name';
    // $request['client_address'] = 'Earth Planet';

    //generate a signature and add it to the array
    ksort($query); //sort alphabetically
    array_unshift($query, 'payment.php');
    array_push($query, '6wfqi40siz3Pw4K8'); //add your secret key (you can take it in your personal cabinet on paybox system)


    $query['pg_sig'] = md5(implode(';', $query));

    unset($query[0], $query[1]);
    $query = http_build_query($query);


    header('Location:https://api.paybox.money/payment.php?' . $query);
    exit;
});
