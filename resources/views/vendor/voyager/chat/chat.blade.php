
@extends('voyager::master')

@section('content')
<div class="container ">
    <div class="row">

        @foreach($chats as $chat)

            <div class="col-lg-12">
                <a href="{{route('showChat',$chat->id)}}">

                    <div class="card p-3">
                        <p>
                            {{$chat['firstMessage']['name'].' : '.$chat['firstMessage']['message']}}
                        </p>
                        <p>
                            {{$chat['firstMessage']['created_at']}}
                        </p>
                    </div>
                </a>
            </div>

        @endforeach
        <div class="col-lg-12">
            {{$chats->links()}}
        </div>
    </div>
</div>
<style>
    .container{
        margin-top:50px;
    }
    .container .col-lg-12 .card{
        padding: 20px;
        display: flex;
        font-size: 20px;
        font-weight: bold;
        color: #000;
        justify-content: space-between;
    }

</style>

@endsection
