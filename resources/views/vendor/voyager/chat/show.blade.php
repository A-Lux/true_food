
@extends('voyager::master')

@section('content')
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('1249ba517dd663de7f44', {
            cluster: 'ap2'
        });
        var doc = document.getElementsByClassName('top-right');

        var channel = pusher.subscribe('App.Chat.{{$chat->id}}');

        channel.bind('pusher:subscription_succeeded', function(members) {


        });

        channel.bind('message-sent', function(data) {
            var objDiv = document.getElementById("card");
            var inputVal = document.getElementById("order_id");
            var node = document.createElement("div");
            var textnode = document.createTextNode(data.message.message)
            node.append(textnode)

            if (inputVal.value == data.message.user_id){

                node.classList.add('myMessage','mine')
            }else{
                node.className += 'myMessage'
            }
            objDiv.appendChild(node)
            var objDiv = document.getElementById("card");
            objDiv.scrollTop = objDiv.scrollHeight;

        });
    </script>
    <input type="hidden" name="order_id" id="order_id" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
    <div class="container ">
        <div class="row">

           <div class="col-lg-12">
                <div class="card messages" id="card">
                    @foreach($messages as $message)
                        <div class="myMessage @if($message->user_id == \Illuminate\Support\Facades\Auth::user()->id) mine @else @endif">
                            {{$message['message']}}
                        </div>
                    @endforeach
                </div>
               <div class="card-form card">
                   <form action="{{route('storeChat')}}" method="post" class="form-inline">
                       {{csrf_field()}}
                       <input type="text" class="form-control" name="message" placeholder="Напишите сообщение">
                       <input type="hidden" name="user_id" value="{{$chat->user_id}}">
                       <input type="hidden" name="chat_id" value="{{$chat->id}}">
                       <input type="submit" class="btn btn-primary" name="submit">
                   </form>
               </div>
           </div>
        </div>
    </div>
    <style>
        .container{
            margin-top:50px;
        }
        .card-form.card{
            min-width: 100%;
        }
        .card-form.card input[type='text']{
            min-width: 90%;
            padding: 2px;
        }
        .container .col-lg-12 .card.messages{
            padding: 20px;
            font-size: 20px;
            font-weight: bold;
            color: #000;
            justify-content: space-between;
        }
        .card.messages{
            max-height: 500px;
            overflow-y: scroll;

        }
        .card .myMessage{
            background-color:#fff;
            color: #000;
            border:1px solid #000;
            max-width: 50%;
            width: 150px;
            margin-top: 10px;

            padding: 10px;
            border-radius: 10px;

            text-align: left;
            word-break: break-all;
        }
        .myMessage.mine{
            color: white;
            margin-left: auto;
            text-align: left;
            background-color:#4c75a3;
        }
    </style>
    <script>
        var objDiv = document.getElementById("card");
        objDiv.scrollTop = objDiv.scrollHeight;
    </script>

@endsection
