@extends('voyager::master')

@section('content')
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>


    <script>
        Pusher.logToConsole = true;

        var pusher = new Pusher('1249ba517dd663de7f44', {
            cluster: 'ap2'
        });
        var doc = document.getElementsByClassName('top-right');

        var channel = pusher.subscribe('Order');

        channel.bind('pusher:subscription_succeeded', function(members) {


        });

        channel.bind('order.created', function(data) {
            $('#order_bodies').prepend('<tr>\n' +
                '\n' +
                '                            <td>\n' +data.order.user_id+
                '                               \n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                ' + data.order.order_type_id+'\n' +
                '                            </td>\n' +
                '                            <td>\n' +data.order.status_id+'\n' +
                '                            </td>\n' +
                '                            <td>\n' + data.order.payment_type_id +'\n' +
                '                            </td>\n' +
                '                            <td>\n' + data.order.delivery_type_id+
                '                                \n' +
                '\n' +
                '                            </td>\n' +
                '                            <td>\n' +data.order.delivery_place_id+'\n' +
                '\n' +
                '                            </td>\n' +
                '                            <td>\n' +data.order.cabinet_number+'\n' +
                '                            </td>\n' +
                '                            <td>\n' +data.order.pick_up_at+'\n' +
                '                            </td>\n' +
                '                            <td>\n' +data.order.phone+'\n' +
                '                            </td>\n' +
                '                            <td>\n' +data.order.email+'\n' +
                '                            </td>\n' +
                '                            <td>\n' +data.order.comment+'\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <a href="/admin/show/'+data.order.id+'" class="btn btn-success">Перейти</a>\n' +
                '                            </td>\n' +
                '\n' +
                '                        </tr>');


        });
    </script>
<div class="container">
    <div class="row">
        <div class="col-lg-12" style="padding-top: 30px;">
            <form action="" method="get">
                <div class="form-group">
                    <input type="text" placeholder="Введите номер заказа" class="form-control" name="order_id">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Найти">
                </div>
            </form>
        </div>
        <div class="col-lg-12">
            <table class="table table-striped">
                <th>
                    <tr>
                        <td>
                            Номер заказа
                        </td>
                        <td>
                            Пользователь
                        </td>
                        <td>
                            Потраченные бонусы
                        </td>
                        <td>
                            Должны оплатить
                        </td>
                        <td>
                            Тип заказа
                        </td>
                        <td>
                            Статус заказа
                        </td>
                        <td>
                            Тип оплаты
                        </td>
                        <td>
                            Тип доставки
                        </td>
                        <td>
                            Место доставки
                        </td>
                        <td>
                            Номер кабинета
                        </td>
                        <td>
                            Забрать через
                        </td>
                        <td>
                            Телефон
                        </td>
                        <td>
                            Почта
                        </td>
                        <td>
                            Комментарий
                        </td>
                    </tr>
                </th>
                <tbody id="order_bodies">
                @foreach($orders as $order)

                        <tr>
                            <td>
                                {{$order->id}}
                            </td>
                            <td>
                                @if($order->user)

                                    {{$order->user->name}}
                                @endif
                            </td>
                            <td>
                                {{$order->used_points}}
                            </td>
                            <td>
                                {{$order->sum}}
                            </td>
                            <td>
                                {{$order->orderType->name}}
                            </td>
                            <td>
                                {{$order->status->name}}
                            </td>
                            <td>
                                {{$order->paymentType->name}}
                            </td>
                            <td>
                                @if($order->deliveryType)

                                    {{$order->deliveryType->name}}
                                @endif

                            </td>
                            <td>

                                @if($order->place)
                                    {{$order->place->name}}
                                @endif
                            </td>
                            <td>
                                {{$order->cabinet_number}}
                            </td>
                            <td>
                                {{$order->pick_up_at}}
                            </td>
                            <td>
                                {{$order->phone}}
                            </td>
                            <td>
                                {{$order->email}}
                            </td>
                            <td>
                                {{$order->comment}}
                            </td>
                            <td>
                                <a href="{{route('show',$order->id)}}" class="btn btn-success">Перейти</a>
                            </td>

                        </tr>

                @endforeach
                </tbody>
            </table>
            {{$orders->links()}}
        </div>

    </div>
</div>

@endsection
