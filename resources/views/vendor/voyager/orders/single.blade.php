@extends('voyager::master')

@section('content')

<div class="container pt-5">
    <div class="row card ">
        <div class="col-md-12 d-flex card_titles">
            <h4>
                @if($order->user)
                        Пользователь : {{$order->user->name}}
                    @endif
            </h4>
            <h4>
                @if($order->place)
                    Адрес : {{$order->place->name}}
                @endif
            </h4>
            <h4>
                Телефон : {{$order->phone}}
            </h4>
            <h4>
                <form action="{{route('updateStatus')}}" method="post">
                    {{csrf_field()}}
                    <div>
                        Статус заказа :
                        <input type="hidden" name="order_id" value="{{$order->id}}">
                        <select name="status_id" id="" >

                            @foreach($statuses as $status)
                                <option value="{{$status->id}}" @if($order->status->id == $status->id) selected @endif>
                                    {{$status->name == 'New order' ? 'Новый заказ' : $status->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-primary">Сохранить</button>
                </form>
            </h4>

        </div>
        <div class="col-lg-12">
            <h4 class="text-center">
                Время доставки: {{$order->pick_up_at}}
            </h4>
        </div>
        @if($order->status_id != 5)

        <div class="col-md-12 d-flex card_titles">
            <a href="{{route('proveOrder',$order->id)}}" class="btn btn-success">
                Завершить
                заказ
            </a>
        </div>
        @endif

        <div class="col-md-12">
            <div class="card">
                <table class="table table-striped">
                    <tr>
                        <td>
                            Имя продукта
                        </td>
                        <td>
                            Цена
                        </td>
                        <td>
                            Количество
                        </td>
    {{--                        <td>--}}
    {{--                            Кэшбэк--}}
    {{--                        </td>--}}
                        <td>
                            Скидка
                        </td>
                    </tr>
                    @foreach($order_details as $order_detail)


                        <tr>
                        <td>
                                {{$order_detail->entity->product->name}}
                            </td>
                            <td>
                                {{$order_detail->entity->price}}
                            </td>
                            <td>
                                {{$order_detail->unit_quantity}}
                            </td>
{{--                            <td>--}}
{{--                                {{$order_detail->entity->cashback}}--}}
{{--                            </td>--}}
                            <td>
                                {{$order_detail->entity->discount}}
                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
</div>
<style>
    .d-flex{
        display: flex;

    }
    .card_titles{
        justify-content: space-evenly;
    }
</style>

@endsection
