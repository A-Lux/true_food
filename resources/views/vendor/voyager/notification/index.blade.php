
@extends('voyager::master')

@section('content')
    <div class="container ">
        <div class="row">
            <div class="col-lg-12">
                <form action="" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="text" class="form-control" name="message" placeholder="Напишите сообщение">
                        <input type="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
            @foreach($notifications as $chat)

                <div class="col-lg-12">


                        <div class="card p-3">
                            <p>
                                Id: {{$chat->id}}
                            </p>
                            <p>
                                Сообщение: {{$chat->message}}
                            </p>
                        </div>

                </div>

            @endforeach
            <div class="col-lg-12">
                {{$notifications->links()}}
            </div>
        </div>
    </div>
    <style>
        .container{
            margin-top:50px;
        }
        .container .col-lg-12 .card{
            padding: 20px;
            display: flex;
            font-size: 20px;
            font-weight: bold;
            color: #000;
            justify-content: space-between;
        }

    </style>

@endsection
