@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/catalog.css') }}">
@endpush

@section('content')
    <div id="catalog" class="spaced">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mb-5">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="/">@lang('menu.main')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('menu.catalog')</li>
                        </ul>
                    </div>
                    <div class="col-xl-12 mb-5">
                        <h1 class="title">{{ $categoryId->name ?? __('menu.catalog') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="my-5">
            <catalog-component is_en="@lang('menu.is_en')" :percent_cashback="`{{setting('site.percent')}}`" :price_asc_lang="{{ json_encode(__('menu.priceAsc')) }}" :price_desc_lang="{{ json_encode(__('menu.priceDesc')) }}" :by_rating_lang="{{ json_encode(__('menu.byRating')) }}" :category="`{{$categoryId->id ?? null}}`" ></catalog-component>
        </section>
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/catalog.js') }}"></script>
@endpush
