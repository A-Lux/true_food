@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/card.css') }}">
@endpush

@section('content')
    <div id="card" class="spaced">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mb-5">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="/">Главная</a>
                            </li>
                            <li class="breadcrumb-item active">{{$productMain->name}}</li>
                        </ul>
                    </div>
                </div>
                <div class="row my-5">
                    <div class="col-xl-2 col-sm-3">
                        <div class="slider-nav">
                            @if($productMain->slider_images != [])
                            @foreach($productMain->slider_images as $image)

                            <div>
                                <img src="{{ asset('/storage/'.$image) }}" alt="">
                            </div>
                            @endforeach
                                @endif

                        </div>
                    </div>
                    <div class="col-xl-6 col-sm-9">
                        <div class="slider-for">

                            @if($productMain->slider_images != [])
                                @foreach($productMain->slider_images as $image)

                                    <div>
                                        <img src="{{ asset('/storage/'.$image) }}" alt="">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                <product-information is_en="@lang('menu.is_en')"  :percent_cashback="`{{setting('site.percent')}}`" :product_main="{{ $productMain }}" :category_name="`{{ $category }}`" :dimension_name="`{{ $dimension }}`"></product-information>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                    <reviews-component is_en="@lang('menu.is_en')" :product_id="{{ $productMain->id }}"></reviews-component>
                    </div>
                </div>
            </div>
        </section>
        @include('partials.popular')
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/reviews.js') }}"></script>
<script src="{{ mix('js/productsCarousel.js') }}"></script>
<script src="{{ mix('js/card.js') }}"></script>
@endpush
