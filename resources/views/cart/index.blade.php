@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/cart.css') }}">
@endpush

@section('content')
    <div id="cart" class="spaced">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mb-5">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="/">@lang('menu.main')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('menu.cart')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="background-image background-repeat background-position background-size py-5" id="re_render" style="--background-image: url({{ asset('/images/cart-image.png') }}); --background-repeat: no-repeat; --background-position: 0 50%; --background-size: auto;">
        <cart-component is_en="@lang('menu.is_en')" ></cart-component>
        </section>
        @include('partials.popular');
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/cart.js') }}"></script>
<script src="{{ mix('js/productsCarousel.js') }}"></script>
@endpush
