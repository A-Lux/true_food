<section class="products popular padding-top padding-bottom" style="--padding-top: 5rem;--padding-bottom: 5rem;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <h1 class="title">@lang('index.popular_meal')</h1>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-xl-12 px-0">
                <div class="products-slider">

                    @foreach($popularProducts as $product)
                        <div class="product-card">
                            <div class="product-header">
                                <a href="{{route('Product',$product->slug)}}">
                                    <img data-lazy="{{ asset('/storage/'.$product->thumbnail) }}" >
                                </a>
                            </div>
                            <div class="product-body p-3 pt-0">
                                <h2 class="name">{{$product->name}}</h2>
                                <p class="description" style="word-break: break-word;">{!! strip_tags($product->description) !!}</p>
                                @if(count($product->variations))
                                <div class="d-flex justify-content-between">
                                    <p class="price">{{$product->variations->first()->price}} @lang('menu.tenge')</p>
                                    <p class="discount">{{setting('site.percent')}} %</p>
                                </div>
                                @endif
                            </div>
                            @if(count($product->variations))
                                <button product-id="{{ $product->id }}" variation-id="{{ $product->variations->first()->id }}" id="cart-btn-popular" class="cart-btn">
                                    <i class="fal fa-shopping-bag"></i>
                                </button>
                            @endif
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
