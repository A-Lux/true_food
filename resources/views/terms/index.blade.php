@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/terms.css') }}">
@endpush

@section('content')
    <div id="terms" class="spaced">
        <section>

            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mb-5">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="/">@lang('menu.main')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('menu.terms')</li>
                        </ul>
                    </div>
                    <div class="col-xl-12 mb-5">
                        <h1 class="title">@lang('menu.terms')</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="my-5">
            <div class="container">
                <div class="row">
                    <?php $local = session()->get('lang') ?>
                    <div class="col-12">
                        {!!  setting('konfidencialnost.terms-'.$local)!!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/terms.js') }}"></script>
@endpush
