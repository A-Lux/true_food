@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/giftcard.css') }}">
@endpush

@section('content')
    <div id="giftcard" class="spaced">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mb-5">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="/">@lang('menu.main')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('menu.giftcard')</li>
                        </ul>
                    </div>
                    <div class="col-xl-12 mb-5">
                        <h1 class="title">@lang('menu.giftcard')</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="background-image background-repeat background-position background-size py-5" style="--background-image: url({{ asset('/images/giftcard-image.png') }}); --background-repeat: no-repeat; --background-position: 0 100%; --background-size: auto;">
            <giftcard-component is_en="@lang('menu.is_en')"></giftcard-component>
        </section>
        @include('partials.popular')
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/giftcard.js') }}"></script>
<script src="{{ mix('js/productsCarousel.js') }}"></script>
@endpush
