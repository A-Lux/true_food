@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/about.css') }}">
@endpush

@section('content')
    @php
    $local = \Illuminate\Support\Facades\App::getLocale();
        @endphp
<div id="about" class="spaced">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <h1 class="title">@lang('menu.about')</h1>
                    @if($local == 'ru')
                    {!! setting('o-nas.about') !!}
                        @else
                        {!! setting('o-nas.abouten') !!}
                        @endif
                </div>
                <div class="col-xl-6">
                    <img src="{{ asset('storage/'.setting('o-nas.image')) }}" alt="About">
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('scripts')
<script src="{{ mix('js/app.js') }}"></script>
@endpush
