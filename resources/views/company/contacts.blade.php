@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/contacts.css') }}">
@endpush

@section('content')
<div id="contacts" class="spaced">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xl-5">
                    <h1 class="title">@lang('menu.contacts')</h1>
                    <div class="contact">
                        <p>@lang('menu.phone'):</p>
                        <a href="tel: {{setting('kontakty.phone')}}">{{setting('kontakty.phone')}}</a>
                    </div>
                    <div class="contact">
                        <p>E-mail:</p>
                        <a href="mailto: {{setting('kontakty.email')}}">{{setting('kontakty.email')}}</a>
                    </div>
                    <input type="hidden" id="longitude" name="longitude" value="{{setting('kontakty.longitude')}}"><input id="latitude" type="hidden" name="latitude" value="{{setting('kontakty.latitude')}}">
                    <div class="contact">

                        <p>@lang('menu.address'):</p>
                        @if(\Illuminate\Support\Facades\App::getLocale() === 'en')
                            <address>{{setting('kontakty.en-address')}}</address>
                            @else

                            <address>{{setting('kontakty.addess')}}</address>
                        @endif

                    </div>
                    <button type="button" data-toggle="modal" data-target="#feedbackModal" class="feedback-btn">@lang('menu.callback')</button>
                </div>
                <div class="col-xl-7">
                    <div id="map" style="width: 100%; height: 500px;"></div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('scripts')
<script src="{{ mix('js/app.js') }}"></script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="{{ mix('js/contacts.js') }}"></script>
@endpush
