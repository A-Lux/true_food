@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('/css/cabinet.css') }}">
@endpush

@section('content')

    <div id="cabinet" class="spaced">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mb-5">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="/">@lang('menu.main')</a>
                            </li>
                            <li class="breadcrumb-item active">@lang('menu.personal')</li>
                        </ul>
                    </div>
                    <div class="col-xl-12 mb-5">
                        <h1 class="title">@lang('menu.personal')</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="background-image background-repeat background-position background-size py-5" style="--background-image: url({{ asset('/images/cabinet-image.png') }}); --background-repeat: no-repeat; --background-position: 0 100%; --background-size: auto;">
            <cabinet-component is_en="@lang('menu.is_en')"></cabinet-component>
        </section>
    </div>
@endsection


@push('scripts')
    <script src="{{ mix('js/cabinet.js') }}"></script>
@endpush
