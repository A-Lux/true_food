@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/home.css') }}">
@endpush

@section('content')

    <div id="home">
        <section>
            <div class="main-slider">
                @foreach($sliders as $slider)
                <div style="--background-image: url({{ asset('storage/'.$slider->slider_image) }})">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-8 d-flex flex-column justify-content-between">
                                <h1>{{$slider['title']}}</h1>
                                <p>{!! $slider['text'] !!}</p>
                                <a href="{{$slider['link']}}">@lang('index.more')</a>

                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </section>
        <section class="background-image background-repeat background-size background-position padding-top padding-bottom" style="--background-image: url({{ asset('images/food-categories.png') }}); --background-repeat: no-repeat; --background-size: auto; --background-position: center; --padding-top: 10rem; --padding-bottom: 10rem;">
            <div class="categories">
                <div class="container">
                    <div class="row mb-3">
                        <div class="col-xl-12 d-flex justify-content-center">
                            <h1 class="title">@lang('index.cat_dishes')</h1>
                        </div>
                    </div>
                    <div class="row mb-5">
                        @foreach($dishCategories as $category)
                            <div class="col-xl-4 col-lg-4 col-md-6 my-3">
                                <a href="{{route('Catalog',$category['slug'])}}">
                                    <div class="food-category" style="--category-image: url('{{asset('storage/'.$category->thumbnail_desktop )}}')">
                                        <h3 class="category-title">{{$category->name}}</h3>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </section>
        <section class="background-image background-repeat background-size background-position padding-top" style="--background-image: url({{ asset('images/beverage-categories.png') }}); --background-repeat: no-repeat; --background-size: auto; --background-position: left; --padding-top: 7rem;">
            <div class="categories">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-7 col-lg-7">
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <h1 class="title">@lang('index.cat_drink')</h1>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($drinkCategories as $category)
                                    <div class="col-xl-6 col-md-6 my-2">
                                        <a href="{{route('Catalog',$category['slug'])}}">
                                            <div class="beverage-category">
                                                <h3 class="category-title">{{$category->name}}</h3>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5">
                            <div class="fuel">
                                <img src="{{ asset('images/fuel.png') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="products padding-top padding-bottom" style="--padding-top: 5rem; --padding-bottom: 5rem;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12">
                        <h1 class="title">@lang('index.today_meal')</h1>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-xl-12 px-0">
                        <div class="products-slider">
                            @foreach($dayProducts as $product)
                            <div class="product-card">
                                <div class="product-header">
                                    <a href="{{route('Product',$product['slug'])}}">
                                        <img data-lazy="{{ asset('/storage/'.$product['thumbnail']) }}">
                                    </a>
                                </div>
                                <div class="product-body p-3 pt-0">
                                    <h2 class="name">{{$product['name']}}</h2>
                                     <p class="description" style="word-break: break-word;">{{strip_tags($product['description'])}}</p>

                                    @if(count($product->variations))
                                    <div class="d-flex justify-content-between">
                                        <p class="price">{{$product->variations->first()->price}} @lang('menu.tenge')</p>
                                        <p class="discount">{{ setting('site.percent') }} %</p>
                                    </div>
                                    @endif
                                </div>
                                @if(count($product->variations))
                                <button product-id="{{ $product->id }}" variation-id="{{ $product->variations->first()->id }}" class="cart-btn">
                                    <i class="fal fa-shopping-bag"></i>
                                </button>
                                @endif
                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include('partials.popular')
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/home.js') }}"></script>
<script src="{{ mix('js/productsCarousel.js') }}"></script>
@endpush
