<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#007eff">
    <title>True Food</title>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    @stack('styles')
</head>
<body>
@php $local = \Illuminate\Support\Facades\App::getLocale(); @endphp
    <div id="app">
        <div id="header">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-sm-8">
                        <div class="sidebar">
                            <div class="controls">
                                <button class="hamburger hamburger--elastic" type="button" aria-controls="navigation">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>

                                <cart-button ></cart-button>
                                <auth-button bill_lang="@lang('menu.bill')"></auth-button>
                            </div>
                            <nav class="main-nav" role="navigation">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <ul>
                                            <li>

                                                <a href="/catalog">@lang('menu.menu')</a>
                                            </li>
                                            <li>
                                                <a href="/about">@lang('menu.about')</a>
                                            </li>
                                            <li>
                                                <a href="/giftcards">@lang('menu.giftcards')</a>
                                            </li>
                                            <li>
                                                <a href="/cart">@lang('menu.cart')</a>
                                            </li>
                                            <li>
                                            <auth-link :auth_link_lang="{{ json_encode(__('menu.auth_link_lang')) }}"  :lang=`{{$local}}` :email_validations="`{{ json_encode(__('validation.custom.email')) }}`" :name_validations="`{{ json_encode(__('validation.custom.name')) }}`"  :password_validations="`{{ json_encode(__('validation.custom.password')) }}`" @if(isset($from_payment)) :another_page="true" @else :another_page="false" @endif></auth-link>
                                            </li>
                                            <li>
                                                <a href="/contacts">@lang('menu.contacts')</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-xl-8 col-8">
                                        <button class="feedback-btn" type="button" data-toggle="modal" data-target="#feedbackModal">@lang('menu.callback')</button>
                                    </div>
                                    <div class="col-xl-4 col-4">
                                        <ul class="lang">
                                            <li class="@if($local == 'ru') active @endif">
                                                <a href="{{route('switch','ru')}}">RU</a>
                                            </li>
                                            <li class="@if($local == 'en') active @endif">
                                                <a href="{{route('switch','en')}}">EN</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-xl-6 col-8">
                                        <ul class="socials">
                                            <li>
                                                <a href="#">
                                                    <i class="fab fa-facebook-f"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fab fa-instagram"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fab fa-vk"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fab fa-twitter"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="offset-xl-1"></div>
                    <div class="col-xl-7 col-md-6 d-flex justify-content-between align-items-center">
                        <a href="/" class="logo">
                            <object data="{{ asset('images/logo.svg') }}"  type="image/svg+xml"></object>
                        </a>
                        <nav class="minor-nav" role="navigation">
                            <ul>
                                <li>
                                    <a href="/catalog">@lang('menu.menu')</a>
                                </li>
                                <li>
                                    <a href="/giftcards">@lang('menu.giftcards')</a>
                                </li>
                                <li>
                                    <a href="/contacts">@lang('menu.contacts')</a>
                                </li>
                            </ul>
                        </nav>
                        <button type="button" class="feedback-btn"  data-toggle="modal" data-target="#feedbackModal">@lang('menu.callback')</button>
                    </div>
                </div>
            </div>
        </div>
        @yield('content')
        <div id="footer" class="padding-top padding-bottom" style="--padding-top: 5rem;--padding-bottom: 5rem;">
            <div class="container">
                <div class="row justify-content-center justify-content-sm-start">
                    <div class="col-xl-2 col-lg-2 col-sm-3 col-10 my-4 my-sm-0 d-flex flex-column justify-content-between">
                        <a href="/" class="logo">
                            <object data="{{ asset('images/logo.svg') }}"  type="image/svg+xml"></object>
                        </a>
                        <p class="copyright">@lang('menu.rights')</p>
                        <ul class="socials">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-vk"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-2 col-sm-4 col-10 d-flex justify-content-center">
                        <nav class="footer-nav" role="navigation">
                            <ul>
{{--                                <li>--}}
{{--                                    <a href="#">@lang('menu.delivery')</a>--}}
{{--                                </li>--}}
                                <li>
                                    <a href="/about">@lang('menu.about')</a>
                                </li>
                                <li>
                                    <a href="/contacts">@lang('menu.contacts')</a>
                                </li>
                                <li>
                                    <a href="/terms">@lang('menu.terms_of_condition')</a>
                                </li>
{{--                                <li>--}}
{{--                                    <a href="#">@lang('menu.bonus')</a>--}}
{{--                                </li>--}}
                            </ul>
                        </nav>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-sm-5 col-10 my-4 my-sm-0 d-flex flex-column justify-content-between">
                        <ul class="contact-details">
                            <li>
                                <a class="phone" href="tel: {{setting('kontakty.phone')}}">{{setting('kontakty.phone')}}</a>
                            </li>
                            <li>
                                <a class="email" href="mailto: {{setting('kontakty.email')}}">{{setting('kontakty.email')}}</a>
                            </li>
                        </ul>
                        <button class="feedback-btn" type="button" data-toggle="modal" data-target="#feedbackModal">@lang('menu.callback')</button>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-sm-12 col-10 my-4 my-lg-0 d-flex flex-column justify-content-between">
                        <div class="apps">
                            <h2>@lang('menu.app')</h2>
                            <p>
                                @lang('menu.app_desc')
                            </p>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-app-store-ios"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-google-play"></i>
                                    </a>
                                </li>
                                <li>
                                    <span class="developed_by">Разработано в
                                        <a href="https://a-lux.kz">A-lux</a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 365px;">
        <div class="modal-content">
            <div class="container py-3 px-5">
                <button type="button" class="close-btn" data-dismiss="modal" aria-label="Close">
                  <i class="fal fa-times"></i>
                </button>
                <div class="row mb-3">
                  <div class="col-xl-12 text-center">
                    <h1>@lang('menu.callback')</h1>
                  </div>
                </div>
                <div class="col-xl-12 text-center">
                    <form id="feedbackForm">
                      <div class="form-group">
                        <label>
                        <span>@lang('menu.name')</span>
                          <input
                            class="form-control"
                            type="name"
                            name="name"
                            placeholder="@lang('menu.name')"
                          />
                        </label>
                      </div>
                      <div
                        class="form-group"
                      >
                        <label>
                        <span>@lang('menu.email')</span>
                          <input
                            class="form-control"
                            type="email"
                            name="email"
                            placeholder="info@mail.com"
                          />
                        </label>
                      </div>
                      <div
                        class="form-group"
                      >
                        <label>
                        <span>@lang('menu.message')</span>
                          <textarea
                            class="form-control"
                            name="message"
                            placeholder="@lang('menu.message')"
                          ></textarea>
                        </label>
                      </div>
                      <div class="form-group">
                        <button type="submit">@lang('menu.send')</button>
                      </div>
                    </form>
                  </div>
            </div>
        </div>
        </div>
    </div>

    <script src="{{ mix('js/minor.js') }}"></script>
    @stack('scripts')
<style>
    @media(max-width: 570px)  {
        #header .minor-nav{
            display: none;
        }
        .col-xl-7.col-md-6.d-flex.justify-content-between.align-items-center{
            justify-content: center !important;
        }
    }
</style>
</body>
</html>
