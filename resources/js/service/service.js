import axios from "axios";
import NProgress from "nprogress";

const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

function getCook() {
    var cookiestring = RegExp(cookiename + "=[^;]+").exec(document.cookie);
    return decodeURIComponent(
        !!cookiestring ? cookiestring.toString().replace(/^[^=]+./, "") : ""
    );
}

const token = localStorage.getItem("TF-XSRF-token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};

apiClient.interceptors.request.use(config => {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(
    response => {
        NProgress.done();
        return response;
    },
    function(error) {
        NProgress.done();
        return Promise.reject(error);
    }
);

export class AuthService {
    static register(data) {
        return apiClient.post("/api/register", data);
    }

    static forget(data){
        return apiClient.post("/api/forget-password",data)
    }
    static login(data) {
        return apiClient.post("/api/login", data);
    }
    static logout() {
        return apiClient.post(`/api/logout`, {}, config);
    }
    static authCheck() {
        return apiClient.get("/api/user", config);
    }
    static putData(data) {
        return apiClient.post(`/api/user`, data, config);
    }
    static getOrders() {
        return apiClient.get("/api/user/orders", config);
    }
    static getOrderProducts(order) {
        return apiClient.get(`/api/user/orders/${order}`, config);
    }
}

export class ProductService {
    static getProducts(parametrs) {
        return apiClient.get(`/api/products`, {
            params: parametrs
        });
    }
    static getFilters(parametrs) {
        return apiClient.get(`/api/filters`, {
            params: parametrs
        });
    }
    static getCategories() {
        return apiClient.get(`/api/categories`);
    }
}

export class CartService {
    static addToCart(parametrs) {
        return apiClient.post(`/api/basket/add`, parametrs, config);
    }
    static getMinMax() {
        return apiClient.get(`/api/order/time-table`);
    }
    static decreaseCount(parametrs) {
        return apiClient.post(`/api/basket/delete`, parametrs);
    }
    static increaseCount(parametrs) {
        return apiClient.post(`/api/basket/increase`, parametrs);
    }
    static getProducts() {
        return apiClient.get(`/api/basket/show`);
    }
    static getPlaces() {
        return apiClient.get(`/api/places`);
    }
    static getDeliveryTypes() {
        return apiClient.get(`/api/order_type`);
    }
    static getPickupPlaces() {
        return apiClient.get(`/api/pickup`);
    }
    static getPaymentTypes() {
        return apiClient.get(`/api/payment_type`);
    }
    static deleteFromCart(parametrs) {
        return apiClient.post(`/api/basket/delete/all`, parametrs);
    }
    static checkout(parametrs) {
        return apiClient.post("/api/orders/pickup", parametrs, config);
    }
}

export class ReviewsService {
    static postReview(parametrs) {
        return apiClient.post("/api/review/store", parametrs);
    }
    static getReviews(product_id) {
        return apiClient.get("/api/review/show", {
            params: { product_id: product_id }
        });
    }
}

export class GiftcardService {
    static getGiftcards() {
        return apiClient.get("/api/gift-cards");
    }
    static sendGiftcard(parametrs) {
        return apiClient.post("/api/orders/gift-card", parametrs, config);
    }
}
