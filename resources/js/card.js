require("./bootstrap.js");
require("slick-carousel");
import { CartService } from "./service/service.js";
import Swal from "sweetalert2";

$(".slider-for").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    autoplay: true,
    autoplaySpeed: 3000,
    fade: true,
    asNavFor: ".slider-nav",
    rows: 0,
    infinite: true
});

$(".slider-nav").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: ".slider-for",
    dots: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    focusOnSelect: true,
    swipeToSlide: true,
    rows: 0,
    infinite: true,
    vertical: true,
    responsive: [
        {
            breakpoint: 576,
            settings: {
                vertical: false
            }
        }
    ]
});

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});
