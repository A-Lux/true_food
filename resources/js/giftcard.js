import Vue from "vue";
import VModal from "vue-js-modal";
import Vuellidate from "vuelidate";
import store from "./vuex/store.js";
import GiftCardComponent from "./components/GiftCardComponent.vue";
import CartButton from "./components/sidebar/CartButton.vue";
import AuthButton from "./components/sidebar/AuthButton.vue";
import AuthLink from "./components/sidebar/AuthLink.vue";

Vue.use(VModal);
Vue.use(Vuellidate);

Vue.component("giftcard-component", GiftCardComponent);
Vue.component("cart-button", CartButton);
Vue.component("auth-button", AuthButton);
Vue.component("auth-link", AuthLink);

const app = new Vue({
    el: "#app",
    store
});
