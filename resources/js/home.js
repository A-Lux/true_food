require("./bootstrap.js");
require("slick-carousel");

$(".main-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    dots: true,
    arrows: false,
    rows: 0
});
