require("./bootstrap.js");
require("bootstrap");
import axios from "axios";
import Swal from "sweetalert2";
import { CartService } from "./service/service.js";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    // timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

window.onload = function() {
    let hamburger = document.querySelector(".hamburger"),
        navigation = document.querySelector(".main-nav"),
        sidebar = document.querySelector(".sidebar"),
        header = document.getElementById("header"),
        spaced = document.querySelector(".spaced");
    if (spaced) {
        spaced.style.paddingTop = header.offsetHeight + 50 + "px";
    }
    hamburger.addEventListener("click", function() {
        this.classList.toggle("is-active");
        navigation.classList.toggle("is-active");
        sidebar.classList.toggle("is-active");
    });
    window.addEventListener("click", function(e) {
        if (
            sidebar.classList.contains("is-active") &&
            hamburger.classList.contains("is-active") &&
            navigation.classList.contains("is-active")
        ) {
            if (
                e.target != hamburger &&
                e.target != navigation &&
                e.target != sidebar
            ) {
                sidebar.classList.remove("is-active");
                navigation.classList.remove("is-active");
                hamburger.classList.remove("is-active");
            }
        }
    });
    var lastScrollTop = 0;
    window.onscroll = function() {
        var st = window.scrollY;
        if (lastScrollTop > st && !sidebar.classList.contains("is-active")) {
            header.classList.remove("header-unstick");
        } else if (
            lastScrollTop < st &&
            !sidebar.classList.contains("is-active")
        ) {
            // header.classList.add("header-unstick");
        }
        lastScrollTop = st;
    };
    const feedbackForm = document.getElementById("feedbackForm");
    let productsDescriptions = document.querySelectorAll(
        ".product-card .description"
    );
    productsDescriptions.forEach(description => {
        if (description.outerText.length > 150) {
            description.innerText =
                description.outerText.substring(0, 150) + "...";
        }
    });
    feedbackForm.addEventListener("submit", function(e) {
        e.preventDefault();
        let data = new FormData(e.target);
        axios
            .post("/api/request", data)
            .then(response => {
                Toast.fire({
                    icon: "success",
                    title: response.data.msg
                });
                $("#feedbackModal").modal("hide");
            })
            .catch(error => {
                Toast.fire({
                    icon: "error",
                    title: error.response.data.msg
                });
            });
    });
    let cartBtns = document.querySelectorAll(".products-slider .cart-btn");
    if (cartBtns.length) {
        cartBtns.forEach(btn =>
            btn.addEventListener("click", function() {
                let parametrs = {
                    variation: btn.getAttribute("variation-id"),
                    product_id: btn.getAttribute("product-id")
                };
                CartService.addToCart(parametrs)
                    .then(response => {
                        let cartComponent = document.getElementById('re_render')
                        let counter = localStorage.getItem('counter')

                        if(!counter){
                            localStorage.setItem('counter',1)
                        }else{
                            counter = parseInt(counter)+1
                            localStorage.setItem('counter',counter)
                        }
                        Toast.fire({
                            icon: "success",

                            title: `<a style="color: #000" href="/cart">${response.data.msg}</a>`
                        });
                    })
                    .catch(error =>
                        {
                            if(error.response.status === 400){
                                window.location.replace(error.response.data.url)
                                return Toast.fire({
                                    icon: "error",
                                    title: error.response.data.msg
                                });
                            }else{
                                return Toast.fire({
                                    icon: "error",
                                    title: error.response.data.msg
                                });
                            }
                        }

                    );
            })
        );
    }

    let cartBtnsPopular = document.querySelectorAll(".products-slider #cart-btn-popular");
    if (cartBtnsPopular.length) {
        cartBtnsPopular.forEach(btn =>
            btn.addEventListener("click", function() {
                let parametrs = {
                    variation: btn.getAttribute("variation-id"),
                    product_id: btn.getAttribute("product-id")
                };
                CartService.addToCart(parametrs)
                    .then(response => {
                        Toast.fire({
                            icon: "success",

                            title: `<a style="color: #000" href="/cart">${response.data.msg}</a>`
                        });
                    })
                    .then(() => {
                        let counter = localStorage.getItem('counter')

                        if(!counter){
                            localStorage.setItem('counter',1)
                        }else{
                            counter = parseInt(counter)+1
                            localStorage.setItem('counter',counter)
                        }
                        Toast.fire({
                            icon: "success",

                            title: `<a style="color: #000" href="/cart">${response.data.msg}</a>`
                        });
                    })
                    .catch(error =>{
                            if(error.response.status === 400){
                                window.location.replace(error.response.data.url)
                                return Toast.fire({
                                    icon: "error",
                                    title: error.response.data.msg
                                });
                            }else{
                                return Toast.fire({
                                    icon: "error",
                                    title: error.response.data.msg
                                });
                            }
                        }
                    );
            })
        );
    }
};
