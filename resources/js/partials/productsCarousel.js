window.$ = window.jQuery = require("jquery");
require("slick-carousel");

$(".products-slider").slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    swipeToSlide: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    dots: false,
    arrows: false,
    rows: 0,
    centerMode: true,
    centerPadding: "100px",
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                centerPadding: "0"
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                centerPadding: "0"
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                centerPadding: "50px"
            }
        }
    ]
});
