import Vue from "vue";
import VModal from "vue-js-modal";
import Vuellidate from "vuelidate";
import Paginate from "vuejs-paginate";
import store from "./vuex/store.js";
import CartButton from "./components/sidebar/CartButton.vue";
import AuthButton from "./components/sidebar/AuthButton.vue";
import AuthLink from "./components/sidebar/AuthLink.vue";
import CatalogComponent from "./components/CatalogComponent.vue";

Vue.use(VModal);
Vue.use(Vuellidate);

Vue.component("cart-button", CartButton);
Vue.component("auth-button", AuthButton);
Vue.component("auth-link", AuthLink);
Vue.component("catalog-component", CatalogComponent);
Vue.component("paginate", Paginate);

const app = new Vue({
    el: "#app",
    store
});
