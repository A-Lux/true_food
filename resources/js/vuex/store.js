import Vue from "vue";
import Vuex from "vuex";
import cart from "./modules/cart.js";
import catalog from "./modules/catalog.js";
import cabinet from "./modules/cabinet.js";
import reviews from "./modules/reviews.js";
import giftcard from "./modules/giftcard.js";

Vue.use(Vuex);

export default new Vuex.Store({
    strict: false,
    modules: {
        cart,
        catalog,
        cabinet,
        reviews,
        giftcard
    }
});
