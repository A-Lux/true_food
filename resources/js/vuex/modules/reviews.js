import { ReviewsService } from "../../service/service.js";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const state = () => ({
    reviews: []
});

const getters = {};

const actions = {
    getReviews({ commit }, product_id) {
        ReviewsService.getReviews(product_id)
            .then(response => {
                commit("SET_REVIEWS", response.data.reviews);
            })
            .catch(error => {
                console.log(error);
            });
    },
    postReview({ commit }, parametrs) {
        ReviewsService.postReview(parametrs)
            .then(response => {
                Toast.fire({
                    icon: "success",
                    title: response.data.msg
                });
                commit("PUSH_REVIEW", response.data.feedback);
            })
            .catch(error => {
                console.log(error);
            });
    }
};

const mutations = {
    PUSH_REVIEW(state, review) {
        state.reviews.push(review);
    },
    SET_REVIEWS(state, reviews) {
        state.reviews = reviews;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
