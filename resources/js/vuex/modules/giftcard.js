import { GiftcardService } from "../../service/service.js";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const state = () => ({
    giftcards: []
});

const getters = {};

const actions = {
    fetchGiftcards({ commit }) {
        GiftcardService.getGiftcards().then(response => {
            commit("SET_GIFTCARDS", response.data.giftCards);
        });
    },
    sendGiftcard({ state }, parametrs) {
        GiftcardService.sendGiftcard(parametrs)
            .then(response => {
                Toast.fire({
                    icon: "success",
                    title: response.data.message
                });
                window.location.replace(response.data.payment_url)
            })
            .catch(error => {
                Toast.fire({
                    icon: "error",
                    title: error.response.data.msg
                });
            });
    }
};

const mutations = {
    SET_GIFTCARDS(state, giftcards) {
        state.giftcards = giftcards;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
