import { AuthService } from "../../service/service.js";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const state = () => ({
    authorized: false,
    phone: "",
    email: "",
    birthday: "",
    name: "",
    avatar: "",
    bill: 0,
    errors: {},
    orders: [],
    orderProducts: {}
});

const getters = {};

const actions = {
    register({ state }, data) {
        AuthService.register(data)
            .then(response => {
                localStorage.setItem(
                    "TF-XSRF-token",
                    response.data.access_token
                );
                window.location.replace("/cabinet");
            })
            .catch(error => {
                if (error.response.status == 422) {
                    state.errors = error.response.data.errors;
                }
            });
    },
    login({ state }, data) {
        AuthService.login(data)
            .then(response => {
                localStorage.setItem(
                    "TF-XSRF-token",
                    response.data.access_token
                );
                window.location.replace("/cabinet");
            })
            .catch(error => {
                Toast.fire({
                    icon: "error",
                    title: error.response.data.message
                });
            });
    },
    forget({state},data){
        AuthService.forget(data)
            .then(response => {
                Toast.fire({
                    icon: "success",
                    title: response.data.message
                });
                window.location.replace("/");
            })
            .catch(error => {
                Toast.fire({
                    icon: "error",
                    title: error.response.data.message
                });
            });
    },
    authCheck({ commit, state }) {
        AuthService.authCheck()
            .then(response => {
                state.authorized = true;
                commit("SET_USERDATA", response.data);
            })
            .catch(error => {
                state.authorized = false;
                if (window.location.pathname.includes("cabinet")) {
                    window.location.replace(window.location.origin);
                }
            });
    },
    fetchOrders({ commit }) {
        AuthService.getOrders()
            .then(response => {
                commit("SET_ORDERS", response.data);
            })
            .catch(error => {
                console.log(error);
            });
    },
    fetchOrderProducts({ commit, state }, order) {
        const orderKeys = Object.keys(state.orderProducts);
        const orderIdx = orderKeys.indexOf(order);
        if (orderIdx > -1) {
            if (state.orderProducts[orderKeys[orderIdx]].length == 0) {
                AuthService.getOrderProducts(order).then(response => {
                    commit("SET_ORDER_PRODUCTS", {
                        products: response.data.details,
                        id: order
                    });
                });
            }
        } else {
            AuthService.getOrderProducts(order).then(response => {
                commit("SET_ORDER_PRODUCTS", {
                    products: response.data.details,
                    id: order
                });
            });
        }
    }
};

const mutations = {
    SET_USERDATA(state, data) {
        state.name = data.name;
        state.email = data.email;
        state.bill = data.bill;
        if (data.phone) {
            state.phone = data.phone;
        }
        if (data.birthday) {
            let birthdate = data.birthday.split(".");
            state.birthday = new Date(birthdate[2], birthdate[1]-1, birthdate[0]);

        }
        if (data.avatar) {
            state.avatar = data.avatar;
        }
    },
    UPDATE_USERDATA(state, { key, value }) {
        state[key] = value;
    },
    SET_ORDERS(state, orders) {
        state.orders = orders;
    },
    SET_ORDER_PRODUCTS(state, { products, id }) {
        state.orderProducts[id] = products;
        state.orderProducts = { ...state.orderProducts };
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
