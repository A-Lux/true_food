import { AuthService, CartService } from "../../service/service.js";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

const state = () => ({
    products: [],
    places: [],
    paymentTypes: [],
    deliveryTypes: [],
    pickupPlaces: [],
    totalPrice: 0,
    pick_up_at: localStorage.getItem("savedPickupAt"),
    min: new Date().toISOString()
});

const getters = {};

const actions = {
    checkout({ state }, parametrs) {
        if (state.pick_up_at) {
            parametrs.append("pick_up_at", state.pick_up_at);
        }
        CartService.checkout(parametrs)
            .then(response => {
                Toast.fire({
                    icon: "success",
                    title: response.data.message
                });
                localStorage.removeItem("savedPaymentType");
                localStorage.removeItem("savedDeliveryPlace");
                localStorage.removeItem("savedMessage");
                localStorage.removeItem("savedCabinetNumber");
                localStorage.removeItem("savedDeliveryType");
                localStorage.removeItem("is_deliverable");
                localStorage.removeItem("savedPickupAt");
                if (response.data.payment_url) {
                    window.location.replace(response.data.payment_url);
                } else {
                    window.location.replace("/cabinet/orders");
                }
            })
            .catch(error => {
                if (error.response.data.errors.pick_up_at) {
                    Toast.fire({
                        icon: "error",
                        title: error.response.data.errors.pick_up_at
                    });
                }
            });
    },
    fetchProducts({ commit }) {

            CartService.getProducts()
            .then(response => {
                commit("SET_PRODUCTS", response.data.basket);
            })
            .catch(error => console.log(error));
    },
    fetchPaymentTypes({ commit }) {
        CartService.getPaymentTypes()
            .then(response => {
                commit("SET_PAYMENTTYPES", response.data.payments);
            })
            .catch(error => {
                console.log(error);
            });
    },
    fetchPlaces({ commit }) {
        CartService.getPlaces()
            .then(response => {
                commit("SET_PLACES", response.data.locations);
            })
            .catch(error => {
                console.log(error);
            });
    },
    fetchDeliveryTypes({ commit }) {
        CartService.getDeliveryTypes()
            .then(response => {
                commit("SET_DELIVERTYPES", response.data.order_types);
            })
            .catch(error => {
                console.log(error);
            });
    },
    fetchPickupPlaces({ commit }) {
        CartService.getPickupPlaces()
            .then(response => {
                commit("SET_PICKUPPLACES", response.data.places);
            })
            .catch(error => {
                console.log(error);
            });
    },
    addToCart({}, parametrs) {
        CartService.addToCart(parametrs)
            .then(response => {
                let counter = localStorage.getItem('counter')

                if(!counter){
                    localStorage.setItem('counter',1)
                }else{
                    counter = parseInt(counter)+1
                    localStorage.setItem('counter',counter)
                }
                Toast.fire({
                    icon: "success",
                    title: `<a style="color: #000" href="/cart">${response.data.msg}</a>`
                });
            })
            .catch(error =>{
                if(error.response.status === 400){
                    window.location.replace(error.response.data.url)
                    Toast.fire({
                        icon: "error",
                        title: error.response.data.msg
                    });
                }else{
                    Toast.fire({
                        icon: "error",
                        title: error.response.data.msg
                    });
                }
            }
            );
    },
    decreaseCount({}, parametrs) {
        CartService.decreaseCount(parametrs).catch(error => {
            console.log(error);
        });
    },
    increaseCount({}, parametrs) {
        CartService.increaseCount(parametrs).catch(error => {
            console.log(error);
        });
    },
    deleteFromCart({ commit }, parametrs) {
        CartService.deleteFromCart(parametrs)
            .then(response => {
                commit("DELETE_PRODUCT", parametrs.product_id);
            })
            .catch(error => console.log(error));
    }
};

const mutations = {
    SET_PRODUCTS(state, products) {
        state.products = products;
    },
    SET_PLACES(state, places) {
        state.places = places;
    },
    SET_PAYMENTTYPES(state, paymentTypes) {
        state.paymentTypes = paymentTypes;
    },
    SET_DELIVERTYPES(state, deliveryTypes) {
        state.deliveryTypes = deliveryTypes;
    },
    SET_PICKUPPLACES(state, pickupPlaces) {
        state.pickupPlaces = pickupPlaces;
    },
    DELETE_PRODUCT(state, id) {
        let index = state.products.findIndex(product => (product.id = id));
        if (index > -1) {
            state.products.splice(index, 1);
        }
    },
    UPDATE_DELIVER_AT(state, pick_up_at) {
        state.pick_up_at = pick_up_at;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
