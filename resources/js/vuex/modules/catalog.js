import { ProductService } from "../../service/service.js";

const state = () => ({
    total: null,
    // filters: [],
    filters: {},
    products: [],
    maxprice: null,
    params: {}
});

const getters = {};

const actions = {
    fetchFilters({ commit }, { parametrs }) {
        ProductService.getFilters(parametrs)
            .then(response => {
                commit("SET_FILTERS", response.data.filters);
                commit("SET_MAXPRICE", response.data.max_price);
                for (const key in response.data.filters) {
                    const element = response.data.filters[key];
                    if (element[0] && element[0].slug) {
                        commit("SET_PARAMS", element[0].slug);
                    }
                }
            })
            .catch(error => {
                console.log(error);
            });
    },
    fetchCategories({ commit }) {
        ProductService.getCategories().then(response => {
            commit("SET_CATEGORIES", response.data);
            commit("SET_PARAMS", "category");
        });
    },
    fetchProducts({ commit }, { parametrs }) {
        ProductService.getProducts(parametrs)
            .then(response => {
                commit("SET_PRODUCTS", response.data.data);
                commit("SET_TOTAL", response.data.meta.last_page);
            })
            .catch(error => {
                console.log(error);
            });
    },
    clearFilters({ commit, state }) {
        for (const key in state.filters) {
            const element = state.filters[key];
            if (element[0].slug) {
                commit("SET_PARAMS", element[0].slug);
            }
        }
    }
};

const mutations = {
    SET_FILTERS(state, filters) {
        state.filters = filters;
    },
    SET_CATEGORIES(state, data) {
        // state.filters[Object.keys(data)[0]] = data[Object.keys(data)[0]];
        state.filters["categories"] = data[Object.keys(data)[0]];
    },
    CLEAR_PARAMS(state) {
        for (var key in state.params) {
            if (state.params.hasOwnProperty(key)) {
                state.params[key] = [];
            }
        }
    },
    SET_PARAMS(state, index) {
        state.params[index] = [];
    },
    SET_PRODUCTS(state, products) {
        state.products = products;
    },
    SET_MAXPRICE(state, price) {
        state.maxprice = price;
    },
    SET_TOTAL(state, total) {
        state.total = total;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
