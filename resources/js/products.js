import Vuex from "vuex";
import { at } from "lodash";
require("./bootstrap");

window.Vue = require("vue");

Vue.use(Vuex);
Vue.component(
    "variation-group",
    require("./components/admin/VariationGroup.vue").default
);

const store = new Vuex.Store({
    state: {
        attributes: []
    },
    getters: {
        attributes: state => {
            return state.attributes;
        }
    },
    mutations: {
        setAttributes(state, attributes) {
            state.attributes = attributes;
        }
    },
    actions: {
        setAttributes({ commit }, attributes) {
            commit("setAttributes", attributes);
        }
    }
});

const app = new Vue({
    el: "#product",
    store
});
