require("./bootstrap.js");

// window.onload = function() {
//     const longitude = document.getElementById("longitude").value;
//     const latitude = document.getElementById("latitude").value;
//     ymaps
//         .load()
//         .then(maps => {
//             const map = new maps.Map("map", {
//                 center: [latitude, longitude],
//                 zoom: 13
//             });
//             let mark = new maps.Placemark(
//                 [latitude, longitude],
//                 {
//                     preset: "islands#redDotIcon"
//                 },
//                 {}
//             );
//             map.geoObjects.add(mark);
//         })
//         .catch(error => console.log("Failed to load Yandex Maps", error));
// };
const longitude = document.getElementById("longitude").value;
const latitude = document.getElementById("latitude").value;
ymaps
    .load()
    .then(maps => {
        const map = new maps.Map("map", {
            center: [latitude, longitude],
            zoom: 13
        });
        let mark = new maps.Placemark(
            [latitude, longitude],
            {
                preset: "islands#redDotIcon"
            },
            {}
        );
        map.geoObjects.add(mark);
    })
    .catch(error => console.log("Failed to load Yandex Maps", error));
