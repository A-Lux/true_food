import Vue from "vue";
import VueRouter from "vue-router";
import VModal from "vue-js-modal";
import Vuellidate from "vuelidate";
import VueTheMask from "vue-the-mask";
import store from "./vuex/store.js";
import CartButton from "./components/sidebar/CartButton.vue";
import AuthLink from "./components/sidebar/AuthLink.vue";
import AuthButton from "./components/sidebar/AuthButton.vue";
import CabinetComponent from "./components/CabinetComponent.vue";
import Profile from "./views/Profile.vue";
import Orders from "./views/Orders.vue";
import { CollapsePlugin } from "bootstrap-vue";

Vue.use(CollapsePlugin);
Vue.use(VModal);
Vue.use(Vuellidate);
Vue.use(VueRouter);
Vue.use(VueTheMask);

Vue.component("cart-button", CartButton);
Vue.component("auth-link", AuthLink);
Vue.component("auth-button", AuthButton);
Vue.component("cabinet-component", CabinetComponent);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/cabinet",
            name: "profile",
            props: true,
            component: Profile
        },
        {
            path: "/cabinet/orders",
            name: "orders",
            props: true,
            component: Orders
        }
    ]
});

const app = new Vue({
    el: "#app",
    store,
    router
});
