<?php

return [
    'more' => 'Order',
    'cat_dishes'=> 'Dishes categories',
    'cat_drink' => 'Drinks categories',
    'today_meal' => 'Meal today',
    'popular_meal' => 'Popular dishes of the week'
];
