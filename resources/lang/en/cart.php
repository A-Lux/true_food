<?php

return [
    "bonuses_lang" => "Cashback from purchase",
    "back_to_shop" => "back to shopping",
    "payment_type" => "Payment type",
    "delivery_type" => "Delivery",
    "delivery_place" => "Delivery place",
    "cabinet_number" => "Enter cabinet number",
    "cabinet_number_required" => "Cabinet number is required",
    "bonus_checked" => "Will written off"
];
