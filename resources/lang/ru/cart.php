<?php

return [
    "bonuses_lang" => "Бонусные баллы с покупки",
    "back_to_shop" => "Вернуться к покупкам",
    "payment_type" => "Способ оплаты",
    "delivery_type" => "Доставка",
    "delivery_place" => "Место доставки",
    "cabinet_number" => "Введите номер кабинета",
    "cabinet_number_required" => "Номер кабинета обязателен",
    "bonus_checked" => "Будет списано"
];
