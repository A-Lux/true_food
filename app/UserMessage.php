<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMessage extends Model
{

    //
    protected $fillable = [
        'user_id',
        'title',
        'message',
        'is_read',
        'chat_id'
    ];
}
