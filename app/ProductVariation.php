<?php

namespace App;

use App\Product;
use App\OrderDetail;
use App\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ProductVariation extends Model
{
    use HasAttributes;
    use Translatable;

    protected $translatable = [
      'sku'
    ];
    protected $fillable = [
        'product_id',
        'sku',
        'price',
        'discount',
        'cashback'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function detail()
    {
        return $this->morpMany(OrderDetail::class, 'entity');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
