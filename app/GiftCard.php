<?php

namespace App;

use App\OrderDetail;
use Illuminate\Database\Eloquent\Model;

class GiftCard extends Model
{
    protected $fillable = [
        'thumbnail',
        'price',
        'from'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'product',
        'attributes'
    ];

    public function detail()
    {
        return $this->morpMany(OrderDetail::class, 'entity');
    }

    public function product()
    {
        return $this->belongsTo(static::class);
    }

    public function attributes()
    {
        return $this->belongsTo(static::class);
    }
}
