<?php

namespace App\Policies;

use App\User;
use App\Order;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Order $order)
    {
        return $user->id == $order->user_id 
            ? Response::allow()
            : Response::deny('You do not own this order.');
    }
}
