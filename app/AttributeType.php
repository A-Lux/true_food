<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeType extends Model
{
    protected $fillable = [
        'name'
    ];
}
