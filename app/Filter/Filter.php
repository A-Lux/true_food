<?php

namespace App\Filter;

use App\Filter\ProductFilter;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

abstract class Filter
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->loadFilters();
    }

    public function apply($builder)
    {
        $queries = collect([]);


        foreach ($this->filters() as $filter => $value) {
            $method = Str::camel($this->filters[$filter]['type']);

            if (method_exists($this, $method)) {
                $queries->push(
                    $this->applyFilter($builder, $method, $value, $this->filters[$filter])
                        ->select('product_variation_id')
                );
            }
        }
        return $queries->reduce(function ($previous, $current) {
            if (is_null($previous)) {
                return $current;
            }
            return $previous->whereIn('product_variation_id', function ($query) use ($current) {
                $query->select('product_variation_id')->fromSub($current, 'product_variation_id_current');
            });
        });
    }

    public function filters()
    {
        return array_filter($this->request->only(array_keys($this->filters)));
    }

    abstract protected function loadFilters();

    protected function applyFilter($builder, $method, $value, $params)
    {
        $method = Str::camel($method);
        return $this->$method($builder, ['value' => $value, 'params' => $params]);
    }
}
