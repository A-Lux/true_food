<?php 
namespace App\Filter;

use App\Attribute;
use App\Filter\Filter;
use App\Filter as FilterModel;

class ProductVariationAttributeFilter extends Filter {
    
    public $filters = [];

    public function checkboxRelation($builder, $params) {
        $valueDimention = $params['value'];
        $filterId = $params['params']['id'];
        $value = $valueDimention;
        
        return $builder->where('specification_id', $filterId)
            ->whereIn('value', $value);
    }
    
    public function checkbox($builder, $params) {
        $filterId = $params['params']['id'];
        foreach($params['value'] as $valueDimention) {
            $valueDimention = explode('_', $valueDimention);
            $value = $valueDimention[0];
            $dimention = null;
            
            if(count($valueDimention) == 2) {
                $dimention = $valueDimention[1];
            }

            $builder = $builder->orWhere(function($query) use($filterId, $value, $dimention) {
                $query = $query->where('attribute_id', $filterId);
                if($dimention) {
                    $query = $query->where('dimention_id', $dimention);
                }
                return $query->where('value', $value);
            });
        }

        return $builder;
    }

    public function singleStrFilter($builder, $value) {
        $filter_id = array_keys($value)[0];
        $value = $value[$filter_id];

        return $builder->where('filter_id', $filter_id)->whereRaw("filter_product.value = '$value'");
    }

    public function singleNumFilter($builder, $value) {
        $filter_id = array_keys($value)[0];
        $value = $value[$filter_id];

        return $builder->where('filter_id', $filter_id)->whereRaw('CAST(filter_product.value as SIGNED) = '. $value);
    }

    public function rangeFilter($builder, $value) {
        $filter_id = explode('_', array_keys($value)[0]);
        $filter_id = array_pop($filter_id);
        $min = $value['min_'.$filter_id] ?: 0;
        $max = $value['max_'.$filter_id] ?: 99999;

        return $builder->where('filter_id', $filter_id)->whereRaw('CAST(filter_product.value as SIGNED) >= '. $min .' AND CAST(filter_product.value as SIGNED)  <= '.$max);
    }

    public function checkboxFilter($builder, $value) {
        $filter_id = array_keys($value)[0];

        return $builder->where('filter_id', $filter_id)->whereIn('value', $value[$filter_id]);
    }

	protected function loadFilters() {
		$filters = Attribute::select('attributes.id', 'slug', 'attribute_types.name as type')
            ->leftJoin('attribute_types', 'attribute_types.id', 'attributes.attribute_type_id')
            ->get();
        
        foreach($filters as $filter) {
            $this->filters[$filter->slug]['id'] = $filter->id;
            $this->filters[$filter->slug]['type'] = $filter->type;
        }
	}
}