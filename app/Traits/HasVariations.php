<?php

namespace App\Traits;

use App\ProductVariation;

trait HasVariations
{
    public function variations()
    {
        return $this->hasMany(ProductVariation::class);
    }

    static public function loadVariations($withAttributes = false, $allAttributes = false)
    {
        return function($query) use($withAttributes, $allAttributes){
            if($withAttributes == true) {
                return ProductVariation::loadAttributes($query, $allAttributes);
            }
            return $query;
        };
    }
}