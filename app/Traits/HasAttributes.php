<?php

namespace App\Traits;

use App\ProductVariationAttribute;

trait HasAttributes
{
    public function allAttributes()
    {
        return $this->hasMany(ProductVariationAttribute::class);
    }

    public function attributes()
    {
        return $this->hasMany(ProductVariationAttribute::class)->where('is_main', 0);
    }

    public function mainAttributes()
    {
        return $this->hasMany(ProductVariationAttribute::class)->where('is_main', 1);
    }

    static public function loadAttributes($query, $allAttributes = false)
    {
        if($allAttributes) {
            return $query->with([
                'allAttributes' => static::loadAttributeWithDimention()
            ]);
        }
        return $query->with([
            'attributes' => static::loadAttributeWithDimention(),
            'mainAttributes' => static::loadAttributeWithDimention()
        ]);

    }

    static public function loadAttributeWithDimention()
    {
        return function($query) {
            $query->with('attribute', 'dimention');
        };
    }
}