<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VariationAttribute extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "attribute_id" => $this->attribute_id,
            "attribute" => $this->attribute->name,
            "value" => $this->value,
            "dimention" => $this->dimention->name,
            "dimention_id" => $this->dimention->id
        ];
    }
}
