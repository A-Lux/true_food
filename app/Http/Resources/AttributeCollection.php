<?php

namespace App\Http\Resources;

use App\Http\Resources\AttributeResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AttributeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = AttributeResource::class;
    
    public function toArray($request)
    {
        return $this->collection;
    }
}
