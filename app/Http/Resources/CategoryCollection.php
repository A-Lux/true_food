<?php

namespace App\Http\Resources;

use App\Http\Resources\Category;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = Category::class;

    public function toArray($request)
    {
        foreach ($this->collection as $item){
             if ($item['thumbnail'] != '')
                $item['thumbnail'] = asset('/storage/'.$item['thumbnail']);
        }
        return $this->collection;
    }
}
