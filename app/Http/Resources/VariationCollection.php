<?php

namespace App\Http\Resources;

use App\Http\Resources\Variation;
use Illuminate\Http\Resources\Json\ResourceCollection;

class VariationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = Variation::class;

    public function toArray($request)
    {
        return $this->collection;
    }
}
