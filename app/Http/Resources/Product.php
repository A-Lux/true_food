<?php

namespace App\Http\Resources;

use App\Http\Resources\Category;
use App\Http\Resources\VariationCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "name" => $this->name,
            "slug" => $this->slug,
            "description" => $this->description,
            "thumbnail" => $this->thumbnail,
            "slider_images" => $this->slider_images,
            "is_popular" => $this->is_popular,
            "is_dish_of_the_week" => $this->is_dish_of_the_week,
            "is_new" => $this->is_new,
            "variations" => new VariationCollection($this->variations),
            "category" => new Category($this->whenLoaded('category'))
        ];
    }
}
