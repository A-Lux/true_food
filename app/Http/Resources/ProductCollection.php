<?php

namespace App\Http\Resources;

use App\Http\Resources\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Cookie;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public $collects = Product::class;
    public  $request;
    public function toArray($request)
    {
        if ($request->byRating)
             $this->collection = $this->collection->sortByDesc('rating');

        foreach ($this->collection as $item){
            $item['variations'][0]['cashback'] =setting('site.percent').' %';
            $item['thumbnail'] = asset('/storage/'.$item['thumbnail']);
            $item['slider_images'] = json_decode($item['slider_images']);
            $images = [];

            if($item['slider_images'] != [])
            foreach ($item['slider_images'] as $image){
                $images[]= asset('/storage/'.$image);
            }
            $item['slider_images'] = $images;

            $cookie = Cookie::get('local');
            if (!$cookie){
                $item['name'] = $item->getTranslatedAttribute('name',$request->local);
                $item['description'] = $item->getTranslatedAttribute('description',$request->local);
            }


            if ($cookie){
                $item['name'] = $item->getTranslatedAttribute('name',$cookie);
                $item['description'] = $item->getTranslatedAttribute('description',$cookie);
            }






            $item->variations->sortBy('price');
        }



        return $this->collection;
    }
}
