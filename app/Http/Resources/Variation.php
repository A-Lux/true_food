<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\VariationAttributeCollection;

class Variation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "sku" => $this->sku,
            "price" => $this->price,
            "discount" => $this->discount,
            "cashback" => $this->cashback,
            "attributes" => new VariationAttributeCollection($this->whenLoaded('attributes')),
            "main_attributes" => new VariationAttributeCollection($this->whenLoaded('mainAttributes')),
            "all_attributes" => new VariationAttributeCollection($this->whenLoaded('allAttributes'))
        ];
    }
}
