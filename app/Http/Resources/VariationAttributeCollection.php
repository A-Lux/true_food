<?php

namespace App\Http\Resources;

use App\Http\Resources\VariationAttribute;
use Illuminate\Http\Resources\Json\ResourceCollection;

class VariationAttributeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = VariationAttribute::class;

    public function toArray($request)
    {
        return $this->collection;
    }
}
