<?php

namespace App\Http\Controllers;

use App\PaymentType;
use Illuminate\Http\Request;

class PaymentTypeController extends Controller
{
    //
    public function show()
    {
        $payments = PaymentType::get();
        return response(['payments' => $payments],200);
    }
}
