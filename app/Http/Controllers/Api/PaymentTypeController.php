<?php

namespace App\Http\Controllers\Api;

use App\PaymentType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class PaymentTypeController extends Controller
{
    //
    public function show()
    {
        $local = Cookie::get('local');
        $payments = PaymentType::get();
        $payments = $payments->translate($payments,$local);
        return response(['payments' => $payments],200);
    }
}
