<?php

namespace App\Http\Controllers\Api;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryCollection;
use Illuminate\Support\Facades\Cookie;

class CategoryController extends Controller
{
    public function getCategories(Request $request)
    {
        $local = Cookie::get('local');
        $category = Category::orderBy('order','asc')->get();
        if(!$local)
            $local = $request->lang;
        $categories = new CategoryCollection($category->translate($category,$local));
        if ($local == 'en' && !$local)
            return response()->json(['Categories'=> $categories]);
        return response()->json(['Категорий'=> $categories]);

    }
}
