<?php

namespace App\Http\Controllers\Api;

use App\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    private $authApi;
    public function __construct()
    {
        $this->authApi = Auth::guard('api');
        $this->middleware(function ($request, $next) {
            if($this->authApi->check()) {
                $request->merge(['user_id' => $this->authApi->user()->id]);
            }
            return $next($request);
        });
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'message' => 'required',
            'attachment' => 'nullable|image'
        ]);
        
        $data = $request->except('attachment');
        if ($request->has('user_id')) {
            $data['user_id'] = $request->user_id;
        }

        if($request->has('attachment')) {
            $filename = 'attachment_'.time().'.'.$request->file('attachment')->extension();
            $data['attachment'] = $request->file('attachment')->storeAs(
                'attachments', $filename
            );
        }

        Feedback::create($data);

        return response(['message' => 'Спасибо за сообщение!'], 201);
    }
}
