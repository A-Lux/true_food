<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RequestController extends Controller
{
    //
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required'
        ]);

        \App\Request::create([
           'name' => $request->name,
           'email' => $request->email,
           'message' => $request->has('message') ? $request->message : '0',
        ]);

        $local = App::getLocale();
        if ($local  == 'ru')
            return response()->json(['msg'=>'Запрос отправлен'],200);
        else
            return  \response()->json(['msg'=> 'Request accepted'],200);
    }
}
