<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Product;
use App\ProductVariation;
use Illuminate\Http\Request;
use App\ProductVariationAttribute;
use App\Http\Controllers\Controller;

class FilterController extends Controller
{
    public function getFilters(Request $request)
    {
        $category = $request->category;
        $attributes = ProductVariationAttribute::select('attributes.name as attribute_name', 'attributes.slug', 'value', 'dimention_id', 'dimentions.name as dimention_name', 'dimentions.short_name as dimention_short_name')
            ->join('dimentions', 'dimention_id', 'dimentions.id')
            ->join('attributes', 'attribute_id', 'attributes.id');
        $maxPrice = ProductVariation::join('products', 'products.id', 'product_id')->orderBy('price', 'desc')->select('price')->first();
        if (!is_null($category)) {
            $attributes = $attributes->whereHas('variation.product', function ($query) use ($category) {
                $query->where('category_id', $category);
            });
            $maxPrice = ProductVariation::join('products', 'products.id', 'product_id')->where('category_id', $category)->orderBy('price', 'desc')->select('price')->first();
        }


        $attributes = $attributes->groupBy('attributes.name', 'attributes.slug', 'value', 'dimention_id', 'dimentions.name', 'dimentions.short_name')
            ->get()
            ->groupBy('attribute_name');


        return response(['filters' => $attributes, 'max_price' => $maxPrice['price']], 200);
    }
}
