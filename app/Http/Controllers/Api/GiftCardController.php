<?php

namespace App\Http\Controllers\Api;

use App\GiftCard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GiftCardController extends Controller
{
    //
    public function show(Request $request)
    {
        $giftCards = GiftCard::where('thumbnail','!=','')->get();

        return response()->json(['giftCards' => $giftCards],200);
    }
}
