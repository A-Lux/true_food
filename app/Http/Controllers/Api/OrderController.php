<?php

namespace App\Http\Controllers\Api;

use App\Events\OrderAdded;
use App\Order;
use App\Paybox\Paybox;
use App\Product;
use App\Status;
use App\GiftCard;
use App\OrderType;
use App\OrderDetail;
use App\PaymentType;
use App\ProductVariation;
use App\User;
use App\UserCard;
use App\UserMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\GiftCardOrdered;
use App\Events\ProductsOrdered;
use App\Order\OrderDetailBuilder;
use App\Order\orOrderBuilder;
use App\Order\DeliveryOrderBuilder;
use App\Order\GiftCardOrderBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    //    private $authApi;
    //    public function __construct()
    //    {
    //        $this->authApi = Auth::guard('api');
    //        $this->middleware(function ($request, $next) {
    //            if($this->authApi->check()) {
    //                $request->merge(['user_id' => $this->authApi->user()->id]);
    //            }
    //            return $next($request);
    //        });
    //    }

    public function pickUp(Request $request)
    {
        $cart = [];


        foreach ($request->cart as $item) {
            $item = json_decode($item);
            $cart[] = $item;
        }
        $request->cart = $cart;
        $local = Cookie::get('local');
        if (!$local){
            if ($request->has('local'))
                $local = $request->local;
        }
        if ($local == 'ru' && !$request->has('pick_up_at')){
            return response()->json(['errors'=>['pick_up_at'=>['Выберите время доставки']]],422);
        }
        if ($local == 'ru' && !$request->has('phone')){
            return response()->json(['errors'=>['phone'=>['Введите номер телефона']]],422);
        }

        if ($local == 'ru' && $request->cabinet_number == '' && $request->delivery_type == 1){
            return response()->json(['errors'=>['pick_up_at'=>['Введите номер кабинета или точный адрес']]],422);
        }
        if ($local == 'en' && $request->cabinet_number == '' && $request->delivery_type == 1){
            return response()->json(['errors'=>['pick_up_at'=>['Cabinet number field required']]],422);
        }

        $validatedData = $request->validate([
            'cart' => 'required',
            'phone' => 'required',
            'comment' => 'nullable',
            'pick_up_at' => 'required',

        ]);

        $user = \auth('api')->user();
        if(!$user)
            return response(['payment_url' => route('auth_required'),'message' => 'Пожалуйста зарегистрируйтесь'],200);
        $orderType = OrderType::where('is_dish', 1)->first();
        $status = Status::where('status_number', 1)->first();
        foreach ($validatedData['cart'] as $item) {
            $item = json_decode($item);
            if ($item->quantity == null && $item->variation_id == null) {
                return  response()->json(['msg' => 'Error in cart'], 400);
            }
        }

        $order = Order::create([
            'user_id' => $user != null ? $user['id'] : null,
            'order_type_id' => $orderType->id,
            'status_id' => $status->id,
            'payment_type_id' => $request->payment_type_id,
            'pick_up_at' => $request->has('pick_up_at') ? $request->pick_up_at : Carbon::now(),
            'phone' => $request->phone,
            'delivery_place_id' => $request->has('delievery_place_id') ? $request->delievery_place_id : null,

            'comment' => $request->has('message') ? $request['message'] : null,
            'cabinet_number' => $request->has('cabinet_number') ? $request->cabinet_number: null,
            'delivery_type_id' => $request->has('delivery_type') ? $request->delivery_type :null,
            'used_points' => $request->has('bonuses') ? $request->bonuses : null,
        ]);



        $claim = 0;
        $description = '';
        $variations = collect([]);
        $totalPrice=  0;
        $totalBonus = 0;
        foreach ($validatedData['cart'] as $cartItem) {
            $cartItem = json_decode($cartItem);
            if ($user)
                $user = User::find($user['id']);
            $variation = ProductVariation::with('attributes.dimention')->find($cartItem->variation_id);


            $product_name = Product::find($variation->product_id)->pluck('name')->first();
            $orderDetail = OrderDetail::create([
                'order_id' => $order->id,
                'entity_type' => 'App\ProductVariation',
                'entity_id' => $variation->id,
                'unit_price' => $variation->price*$cartItem->quantity,
                'unit_quantity' => $cartItem->quantity,
                'unit_cashback' => $variation->cashback,
                'comment' => isset($request['comment']) ? $request['comment'] : null
            ]);
            $description .= $product_name . ',';
            $variations->push([
                'variation' => $variation,
                'detail' => $orderDetail
            ]);
            $totalPrice += $cartItem->quantity*$variation->price;
            $claim += $variation['cashback'];
            $totalBonus += $variation->cashback;

        }

        if($request->has('bonuses'))
        {
            $bill = $user->bill;
            $total = $totalPrice;
            $totalPrice -= $request->bonuses;
            $bill -= $request->bonuses;
            if ($bill <0){
                return response()->json(['msg' => 'Not enough']);
            }
        }


        if($totalPrice <= 0){
            if ($total>$request->bonuses){
                $user->bill -= $request->bonuses;
                $user->save();
            }else{
                $user->bill -= $total;
                $order->status_id = 6;
                $user['bill'] += 0;
                $user->save();
                $order->save();

            }

        }else{
            $user->bill -= $request->bonuses;
            $user->save();
        }

        if ($order->payment_type_id == 1 && $totalPrice>0 && $request->has('user_card_id') && $request->user_card_id != ''){
            $user_card = UserCard::find($request->user_card_id);
            $query = [
                'pg_merchant_id' => 532183,
                'pg_amount' => $totalPrice,
                'pg_order_id' => strval($order->id),
                'pg_user_id' => strval($user->id),
                'pg_card_id' => $user_card->card_id,
                'pg_description' => 'Номер заказа '.$order->id,
                'pg_result_url' => route('orderStatus'),
                'pg_success_url' => route('Personal').'/orders',
                'pg_failure_url' => route('Personal').'/orders',
                'pg_salt' => Str::random(15),

            ];
            ksort($query);
            array_unshift($query,'init');
            array_push($query, '6wfqi40siz3Pw4K8');
            $query['pg_sig'] = md5(implode(';',$query));
            unset($query[0],$query[1]);
            $response = Http::withHeaders([
                'X-Content-Type'=> 'multipart/form-data'
            ])->post('https://api.paybox.money/v1/merchant/532183/card/init',$query);
            $xml = new \SimpleXMLElement($response->body());
            $xml = (array) $xml;
            $save_query = [
                'pg_merchant_id' => 532183,
                'pg_payment_id' => $xml['pg_payment_id'],
                'pg_salt' => Str::random(15),
            ];

            ksort($save_query);
            array_unshift($save_query,'pay');
            array_push($save_query,'6wfqi40siz3Pw4K8');
            $save_query['pg_sig'] = md5(implode(';',$save_query));
            unset($save_query[0],$save_query[1]);
            $pay_response = Http::withHeaders([
                'X-Content-Type'=> 'multipart/form-data',
            ])->post('https://api.paybox.money/v1/merchant/532183/card/pay',$save_query);

            if ($pay_response->status() == 200){
                if ($request->has('local') && $request->local == 'en' || $local == 'en'){
                    return response( ['response'=>$pay_response->body(),'message' => 'Paid up'], 201);
                }


                return response(['response'=>$pay_response->body(),'message' => 'Оплачено'], 201);
            }
        }

        if ($order->payment_type_id == 1 && $totalPrice>0) {
            $query = [
                'pg_merchant_id'=> 532183,
                'pg_amount' => $totalPrice,
                'pg_salt' => Str::random(15),
                'pg_order_id'=>$order->id,
                'pg_description' => 'Номер заказа '.$order->id,
                'pg_result_url' => route('orderStatus'),
                'pg_success_url' => route('Personal').'/orders',
                'pg_success_url_method' => 'AUTOGET',

            ];


            $query['pg_testing_mode'] = 0; //add this parameter to request for testing payments
            ksort($query); //sort alphabetically
            array_unshift($query, 'payment.php');
            array_push($query, '6wfqi40siz3Pw4K8'); //add your secret key (you can take it in your personal cabinet on paybox system)
            $query['pg_sig'] = md5(implode(';', $query));
            unset($query[0], $query[1]);
            $query = http_build_query($query);
            $local = Cookie::get('local');
            if ($request->has('local') && $request->local == 'en' || $local == 'en'){
                return response(['payment_url' => 'https://api.paybox.money/payment.php?'.$query, 'message' => 'Moving on to the payment process'], 202);
            }


            return response(['payment_url' => 'https://api.paybox.money/payment.php?'.$query, 'message' => 'Переходим к процессу оплаты'], 202);
        }


        event(new OrderAdded($order));
        event(new ProductsOrdered($variations, $order));
        try {
            $admin = setting('site.order_email');
            if($admin){
                Mail::send('emails.admin.order_added', ['order' => $order,'admin'=>$admin ], function ($m) use ($admin,$order) {
                    $m->from('truefood.kz@gmail.com', 'truefood.kz');

                    $m->to($admin)->subject('Оффлайн заказ номер '.$order->id);
                });
            }
        }catch (\Exception $e){
            return response(['message' => 'Error' .$e], 500);
        }
        $cookie = Cookie::forget('basket');
        $local = Cookie::get('local');
        if ($request->has('local') && $request->local == 'en' || $local == 'en') {
            return response(['message' => 'Your order has been accepted for processing!'], 200)->withCookie($cookie);

        }
        return response(['message' => 'Ваш заказ принят в обработку!'], 200)->withCookie($cookie);

    }

    public function delivery(Request $request)
    {
        $validatedData = $request->validate([
            'cart' => 'required',
            'payment_type_id' => 'required',
            'delivery_place_id' => 'required',
            'pick_up_at' => 'nullable',
            'cabinet_number' => 'required',
            'phone' => 'requred',
            'comment' => 'nullable'
        ]);
        foreach ($validatedData['cart'] as $item) {
            if ($item->quantity == null && $item->variation_id == null) {
                return  response()->json(['msg' => 'Error in cart'], 400);
            }
        }
        $orderType = OrderType::where('is_dish', 1)->first();
        $status = Status::where('status_number', 1)->first();
        $user = \auth('api')->user();
        $order = Order::create([
            'user_id' => $user != null ? $user['id'] : null,
            'order_type_id' => $orderType->id,
            'status_id' => $status->id,
            'payment_type_id' => $request->payment_type_id,
            'delivery_place_id' => $request->delivery_place_id,
            'pick_up_at' => $request->has('pick_up_at') ? $request->pick_up_at : null,
            'cabinet_number' => $request->cabinet_number,
            'phone' => $request->phone,
            'location_id' => $request->has('location_id') ? $request->location_id : null,
            'comment' => $request->has('comment') ? $request->comment : null,
            'delivery_type_id' => $request->delivery_type_id
        ]);
        $description = '';

        $variations = collect([]);
        foreach ($validatedData['cart'] as $cartItem) {
            $variation = ProductVariation::with('attributes.dimention')->find($cartItem['variation_id']);

            $orderDetail = OrderDetail::create([
                'order_id' => $order->id,
                'entity_type' => 'App\ProductVariation',
                'entity_id' => $variation->id,
                'unit_price' => $variation->price,
                'unit_cashback' => $variation->cashback,
                'unit_quantity' => $cartItem['quantity'],
                'comment' => isset($cartItem['comment']) ? $cartItem['comment'] : null
            ]);
            $product_name = Product::find($variation->product_id)->pluck('name')->first();

            $description .= $product_name . ',';
            $variations->push([
                'variation' => $variation,
                'detail' => $orderDetail
            ]);
        }

        if ($order->payment_type_id == 1) {
            $pay = new Paybox();
            $pay->setPgMerchantId(532183);
            $pay->setPgAmount($request->totalPrice);
            $pay->setPgSalt(Str::random(15));
            $pay->setPgOrderId($order->id);
            $pay->setPgDescription($description);
            $pay->setPgFailureUrl(route('order.failure'));
            $pay->setPgSuccessUrl(route('Personal').'/orders');
            $pay->setPgResultUrl(route('orderStatus'));
            $sd = 'sds';
            return response(['payment_url' => $pay->pay(), 'message' => 'Ваш заказ принят в обработку'], 201);
        }

        event(new ProductsOrdered($variations, $order));
        event(new OrderAdded($order));
        $local = App::getLocale();

        return response(['message' => setting('soobshheniya.order')], 201);

    }

    public function giftCard(Request $request)
    {
        $validatedData = $request->validate([
            'gift_card_id' => 'nullable|exists:App\GiftCard,id',
            'price' => 'required_without:gift_card_id',
            'email' => 'required',
            'comment' => 'nullable'
        ]);

        $orderType = OrderType::where('is_dish', 0)->first();
        $status = Status::where('status_number', 1)->first();
        $paymentType = PaymentType::where('name', 'Онлайн')->first();

        if ($request->has('gift_card_id')) {
            $giftCard = GiftCard::find($request->gift_card_id);

            if ($request->user('api')){
                $giftCard->update([
                    'from' => $request->user('api')->name
                ]);
            }
            if ($request->has('name') && $request->name != ''){
                $giftCard->update([
                   'from' => $request->name
                ]);
            }



        } else {
            if ($request->has('name') || $request->user('api')){
                if ($request->has('name') && $request->name != ''){

                    $giftCard = GiftCard::create([
                        'price' => $request->price,
                        'thumbnail'=>'',
                        'is_active' => 0,
                        'from' => $request->has('name') ? $request['name'] : null,
                    ]);
                }else{

                    $user = $request->user('api');
                    $giftCard = GiftCard::create([
                        'price' => $request->price,
                        'thumbnail'=>'',
                        'is_active' => 0,
                        'from' =>$user->name
                    ]);
                }
            }else{
                $giftCard = GiftCard::updateOrCreate(
                    ['price' => $request->price],
                    ['price' => $request->price]
                );
            }

        }



        $order = Order::create([
            'user_id' => $request->has('user_id') ? $request->user_id : null,
            'order_type_id' => $orderType->id,
            'status_id' => $status->id,
            'email' => $validatedData['email'],
            'payment_type_id' => $paymentType->id,
            'comment' => $request->has('comment') ? $request->comment : null,
        ]);

        $orderDetail = OrderDetail::create([
            'order_id' => $order->id,
            'entity_type' => 'App\GiftCard',
            'entity_id' => $giftCard->id,
            'unit_price' => $giftCard->price,
            'unit_quantity' => 1,
            'comment' => $request->has('comment') ? $request->comment : null
        ]);


        $user = User::where('email',$request->email)->first();

        if (!$user){
            return  response()->json(['msg' => 'Пользователь с такой почтой не найден'],404);

        }





            $query = [
                'pg_merchant_id'=> 532183,
                'pg_amount' => $giftCard->price,
                'pg_salt' => Str::random(15),
                'pg_order_id'=>$order->id,
                'pg_description' => $order->email,
                'pg_result_url' => route('giftCardResult'),
                'pg_success_url' => route('Personal').'/orders',
                'pg_success_url_method' => 'AUTOGET',

            ];

            $query['pg_testing_mode'] = 0; //add this parameter to request for testing payments


            ksort($query); //sort alphabetically
            array_unshift($query, 'payment.php');
            array_push($query, '6wfqi40siz3Pw4K8'); //add your secret key (you can take it in your personal cabinet on paybox system)


            $query['pg_sig'] = md5(implode(';', $query));

            unset($query[0], $query[1]);

            $query = http_build_query($query);

//redirect a customer to payment page


            $cookie = Cookie::forget('basket');
            return response(['payment_url' => 'https://api.paybox.money/payment.php?'.$query, 'message' => 'Ваш заказ принят в обработку'], 201)->withCookie($cookie);


    }

    public function orderStatus(Request $request)
    {
        if ($request->pg_result == 1){
            $order = Order::find($request->pg_order_id);
            $order['status_id'] = 6;
            $order_details = OrderDetail::where('order_id',$order->id)->get();
            $total_cash = 0;
            $total_main = 0;
            foreach ($order_details as $detail){
                $total_main += $detail['unit_price'];
                $total_cash += $detail['unit_cashback'];
            }
            $user = User::find($order->user_id);

            $user['bill'] += (setting('site.percent')/100)*($total_main-$order['used_points']);
            $user->save();
            $order->save();
            $cookie = Cookie::forget('basket');
            try {
                $admin = setting('site.order_email');
                if($admin){
                    Mail::send('emails.admin.order_added', ['order' => $order,'admin'=>$admin ], function ($m) use ($admin,$order) {
                        $m->from('truefood.kz@gmail.com', 'truefood.kz');

                        $m->to($admin)->subject('Онлайн оплата заказ номер '.$order->id);
                    });
                }
            }catch (\Exception $e){
                Log::error('Error email order '.$e);
            }
            return response()->json(['msg'=> '200'],200)->withCookie($cookie);
        }
    }

    public function giftCardResult(Request $request)
    {
        if ($request->pg_result == 1){
            $order = Order::find($request->pg_order_id);
            $order_detail = OrderDetail::where('order_id',$order->id)->first();
            $giftCard = GiftCard::find($order_detail['entity_id']);
            if ($request->pg_description && $order->order_type_id == 2){
                event(new GiftCardOrdered($order, $order_detail));

                $user = User::where('email',$order->email)->first();
                $user['bill'] += $giftCard['price'];

                $user->save();
                return response()->json(['msg'=> '200'],200);

            }
        }
    }
    public function getOrderCount()
    {
        $orderCount = Order::where(function ($query){
            $query->where('payment_type_id','!=',1);
            $query->where('status_id','!=',5);
        })->orWhere(function ($query){
            $query->where('payment_type_id',1);
            $query->where('status_id', 6);
        })->where('order_type_id',1)->count();

        $chatCount = UserMessage::where('is_read',0)->count();
        $requestCount = \App\Request::where('is_read',0)->count();
        return response()->json(['orderCount'=>$orderCount, 'chatCount' => $chatCount, 'requestCount'=> $requestCount],200);
    }

    public function timeTable()
    {
        $time_table = [];
        if (setting('kontakty.work_time'))
            $time_table['work_time'] = setting('kontakty.work_time');
        if (setting('kontakty.work_time_end'))
            $time_table['work_time_end'] = setting('kontakty.work_time_end');
        if (date('D') == 'Sat' || date('D') == 'Sun'){
            if (setting('kontakty.weekend_time_end'))
                $time_table['work_time']  = setting('kontakty.weekend_time_start');
            if (setting('kontakty.weekend_time_start'))
                $time_table['work_time_end'] = setting('kontakty.weekend_time_end');
        }
        if (setting('kontakty.weekend_time_end'))
            $time_table['work_time_week']  = setting('kontakty.weekend_time_start');
        if (setting('kontakty.weekend_time_start'))
            $time_table['work_time_end_week'] = setting('kontakty.weekend_time_end');

        if  (setting('kontakty.work_in_saturday')){
            $time_table['work_in_saturday'] = setting('kontakty.work_in_saturday');
        }else{
            $time_table['work_in_saturday'] = 0;
        }

        return $time_table;
    }
}
