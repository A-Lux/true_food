<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\User;
use App\UserDeviceToken;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ]);
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $user->save();

        $tokenResult = $user->createToken('Laravel Personal Access Client');
        $token = $tokenResult->token;
        $chat = Chat::create(['user_id'=>$user['id'],'admin_id'=>1]);
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        if ($request->has('device_token')) {


            $device_token = UserDeviceToken::where('device_token', $request->device_token)->first();
            if (!$device_token)
                UserDeviceToken::create([
                    'user_id' => $user->id,
                    'device_token' => $request->device_token
                ]);
        }
        $token->save();
        $cookie = Cookie::get('basket');
        if ($cookie)
            return response()->json([
                // 'redirect_url' => route('cabinet'),
                'chat_id' => $chat['id'] ,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ], 201)->withCookie(Cookie::forget('basket'));
        return response()->json([
            // 'redirect_url' => route('cabinet'),
            'chat_id' => $chat['id'] ,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 201);
    }
}
