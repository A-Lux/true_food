<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\OrderDetail;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function user(Request $request)
    {
        $user = $request->user()->load('location');
        $user['avatar'] = asset('/storage/'.$user['avatar']);


        return response()->json($user);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'string|min:3',
            'middlename' => 'string|min:3',
            'lastname' => 'string|min:3',
            'birthday' => 'nullable|date',
            'password' => 'nullable|string|min:6',
            'email' => 'email|unique:users,email,' . $request->user()->id,
            'location_id' => 'nullable|exists:App\Location,id',

        ]);
        if ($request->avatar ){
            $user = $request->user();
            $user->avatar = $request->file('avatar')->store('users','public');
            $user->save();
        }

        if ($request->avatar == null && $request->has('new')){
            $user = $request->user();
            $user->avatar = '';
            $user->save();
        }

        $userData = $request->except('password_confirmation');
        if ($request->password) {
            $userData['password'] = Hash::make($request->password);
        }


        $request->user()->update($userData);
        if ($request->birthday != ''){
            $birthday = str_replace('/','.',$request->birthday);
            $birthArray = explode('.',$birthday);
            $firstElement = $birthArray[0];
            $secondElement = $birthArray[1];
            $birthArray[0] = $secondElement;
            $birthArray[1] = $firstElement;
            $birthString = implode('.',$birthArray);
            $user = $request->user();
            $user->birthday = $birthString;
            $user->save();

        }

        return response()->noContent();
    }

    public function orders(Request $request)
    {
        $local = Cookie::get('local');
        if ($request->has('local')){
            $local = $request->local;
        }

        $orders = $request->user()
            ->orders()
            ->select(
                'order_id',
                'status_id',
                'orders.created_at',
                DB::raw('SUM(unit_price) as total_price'),
                DB::raw('SUM(unit_cashback) as total_cashback')
            )
            ->join('order_details', 'order_details.order_id', 'orders.id')
            ->groupBy('order_details.order_id')
            ->orderBy('order_id','desc')
            ->get();
        foreach ($orders as $order){
            $status = Status::find($order['status_id']);
            $status = $status->translate($status,$local);
            $order['total_cashback'] = setting('site.percent'). ' %';
            $order['status'] = $status;
        }
        return response($orders);
    }

    public function order(Request $request, Order $order)
    {
        $response = Gate::inspect('view', $order);

        if ($response->allowed()) {
            $order->load(['details.entity' => function ($query) {
                return $query->with([
                    'product', 'attributes' => function ($attributesQuery) {
                        return $attributesQuery->with('attribute', 'dimention');
                    }
                ]);
            }]);
            foreach ($order['details'] as $detail){
                $detail['unit_cashback'] = $detail['unit_price']*(setting('site.percent')/100); 
            }
            $order['status'] = Status::find($order['status_id']);
            if ($request->has('local')){
                $local = $request->local;
                $order['status'] = $order['status']->translate($order['status'],$local);
            }

            return response($order);
        } else {
            return response($response->message(), 403);
        }
    }

    public function forgetPassword(Request $request)
    {
        if(!$request->has('email')){
            if ($request->local == 'en')
                return response()->json(['message' => 'Please enter email field'],422);

            return response()->json(['message' => 'Пожалуйста введите почту'],422);


        }

        $user = User::where('email',$request->email)->first();
        if(!$user){
            return response()->json(['message'=> 'Пользователь с такой почтой не существует'],422);
        }
        $password = Str::random(10);
        $user['password'] = Hash::make($password);
        $user->save();

        try {
            Mail::send('emails.forget-password',['user' => $user,'password' => $password],function ($m) use ($user){
                $m->from('truefood.kz@gmail.com', 'truefood.kz');
                $m->to($user->email)->subject( 'Восттановление пароля пользователя');

            });
            if ($request->local == 'en')
                return response()->json(['message'=>'We send password to your email '.$user->email],200);

            return response()->json(['message'=>'Мы прислали вам пароль на почту '.$user->email],200);
        }catch (\Exception $e){
            Log::error('Email error password'. $e);
            if ($request->local == 'en'){
                return response()->json(['message' => 'Error'],500);

            }
            return response()->json(['message' => 'Ошибка'],500);

        }

    }

    public function socials()
    {
        $data['vk_link'] = setting('kontakty.vk_link');
        $data['instagram_link'] = setting('kontakty.instagram_link');
        $data['facebook_link'] = setting('kontakty.facebook_link');
        $data['twitter_link'] = setting('twitter_link');

        return response(['data'=> $data],200);
    }
}
