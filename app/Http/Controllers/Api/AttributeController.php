<?php

namespace App\Http\Controllers\Api;

use App\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AttributeCollection;

class AttributeController extends Controller
{
    public function getAttributes()
    {
        $attributes = Attribute::with('dimentionType.dimentions')->get();
        
        return response(new AttributeCollection($attributes));
    }
}
