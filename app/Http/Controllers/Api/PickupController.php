<?php

namespace App\Http\Controllers\Api;

use App\Location;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PickupController extends Controller
{
    //
    public function show()
    {
        $locations = Location::get();

        return response(['places' => $locations],200);
    }
}
