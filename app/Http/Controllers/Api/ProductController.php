<?php

namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\VariationCollection;
use App\Http\Resources\Product as ProductResource;

class ProductController extends Controller
{
    public function show(Product $product, Request $request)
    {
        $product->load(['variations' => Product::loadVariations($withAttributes = true), 'category']);
        $product->slider_images = json_decode($product->slider_images);
        if ($request->lang){
            $product['name'] = $product->getTranslatedAttribute('name',$request->lang);
            $product['description'] = $product->getTranslatedAttribute('description',$request->lang);
        }
        foreach ($product['variations'] as $variation){
            $variation['cashback'] = setting('site.percent').'%';
        }

        return new ProductResource($product);
    }

    public function editShow($id)
    {
        $product = Product::find($id);
        if ($product['is_show'] == 1){
            $product['is_show'] = 0;
        }else{
            $product['is_show'] = 1;
        }

        $product->save();

        return back()->with(['message' => 'Статус обновлен']);
    }
    public function variations(Product $product)
    {
        try{
            $product->load(['variations' => Product::loadVariations($withAttributes = true, $allAttributes = true)]);

            return new VariationCollection($product->variations);
        }catch (Exception $e){
            return response(['message'=>'Error '.$e]);
        }
    }
}
