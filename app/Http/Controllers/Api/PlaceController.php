<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\DeliveryPlace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class PlaceController extends Controller
{
    //
    public function show(Request $request)
    {
        $local = Cookie::get('local');
        $locations = DeliveryPlace::get();

        if ($local)
            $locations = $locations->translate($locations,$local);

        if ($request->local)
            $locations = $locations->translate($locations,$request->local);

        return response()->json(['locations' => $locations], 200);
    }
}
