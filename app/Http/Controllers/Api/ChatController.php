<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\Events\ChatCreated;
use App\Events\MessageSent;
use App\Http\Controllers\Controller;
use App\User;
use App\UserMessage;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ChatController extends Controller
{
    //
    protected $serverKey = 'AAAAdc7Xf4g:APA91bH_PvWK5vt39Q8jBEIKWCAUZ2I9q_H_r3C2fQQK-15A6vy9a-RAE_5Lkfl4sjrtqojO2Vg2BAYjo1x2a_HPRlOwNYBm-62N28SckxDuqYIij_SWs1gpynhjqE9-OUzoNW3GKTzP';

    public function show(Request $request)
    {
        $messages = Chat::where('user_id',$request->user()->id)->orderBy('id','desc')->get();
        foreach ($messages as $message){
            $message['is_read'] = 1;
            $message->save();
        }
        return response()->json(['messages' => $messages],200);
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'message' => 'required',

        ]);
        $user = auth('api')->user();
        $chat =  Chat::where('user_id',$user->id)->first();
        if ($request->has('chat_id') || $chat){


            $message = UserMessage::create([
                'user_id' => $user ? $user->id : null,
                'message' => $request->message,
                'title' => $request->has('image') ? $request->file('image')->store('image'):0,
                'is_read' => 0,
                'chat_id' => $chat->id
            ]);
            $admin = User::find(1);
            try{
                Mail::send('emails.chat', ['chat' => $chat,'admin'=>$admin->email ], function ($m) use ($admin,$chat) {
                    $m->from('truefood.kz@gmail.com', 'truefood.kz');

                    $m->to($admin->email)->subject('Вам пришло сообщение '.$chat->id);
                });
            }catch (\Exception $e){
                Log::info('Error '.$e);
            }


            event(new MessageSent($message,$chat->id));
            return response(['message' =>$message, 'chat' =>$chat],200);
        }else{
            $admins = User::where('role_id',1)->count();
            $chat = Chat::create([
                'user_id' => $user ? $user->id : null,
                'admin_id' =>1,
            ]);
            $message = UserMessage::create([
                'user_id' => $user ? $user->id : null,
                'message' => $request->message,
                'title' => $request->has('image') ? $request->file('image')->store('image'): 0,
                'is_read' => 0,
                'chat_id' => $chat->id
            ]);
            $admin = User::find(1);

            event(new MessageSent($message,$chat->id));
            event(new ChatCreated($user->id,$chat,$message));
            Mail::send('emails.chat', ['chat' => $chat,'admin'=>$admin ], function ($m) use ($admin,$chat) {
                $m->from('truefood.kz@gmail.com', 'truefood.kz');

                $m->to($admin)->subject('Вам пришло сообщение  '.$chat->id);
            });
            return  response()->json(['message' => $message, 'chat'=>$chat],200);
        }
    }

    public function allMessages(Request $request)
    {

        if ($request->chat){

            $messages = UserMessage::where('chat_id',$request->chat)->orderBy('id','asc')->get();
            $chat = Chat::find($request->chat);
            foreach ($messages as $message){
                $message['is_read'] = 1;
                $message->save();
            }
            return response()->json([
                'messages'=> $messages,
                'chat' => $chat
            ],200);
        }
        return \response()->json(['msg'=> 'Not found'],404);
    }

    public function getCount()
    {
        $messagesCount = UserMessage::where('is_read',0)->count();

        return \response()->json(['count' => $messagesCount],200);
    }
}
