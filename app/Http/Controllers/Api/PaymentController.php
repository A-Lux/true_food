<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\OrderDetail;

use App\User;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    //
    public function orderStatus(Request $request)
    {
        $order = $request->order;
        if ($order->status == 1){
            $order_details = OrderDetail::get();
            $sumCash = 0;
            foreach ($order_details as $order_detail){
                $sumCash += $order_detail['unit_cachback'];
            }
            if ($order['user_id'] != null){
                $user = User::find($order['user_id']);
                $user['bill'] += $sumCash;
                $user->save();
            }
            return response(['msg' => 'ok'],200);

        }
    }
}
