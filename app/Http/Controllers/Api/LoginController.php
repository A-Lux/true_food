<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\UserDeviceToken;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = $request->only(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Неверный логин или пароль'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Laravel Personal Access Client');
        $token = $tokenResult->token;

        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();
        $chat = Chat::where('user_id',$user['id'])->first();
        if (!$chat)
            $chat = Chat::create(['user_id'=>$user['id'],'admin_id'=>1]);

        if ($request->has('device_token') && $request->device_token != '') {



            $device_token = UserDeviceToken::where('device_token', $request->device_token)->first();
            if (!$device_token)
                UserDeviceToken::create([
                    'user_id' => $user->id,
                    'device_token' => $request->device_token
                ]);
        }
        $cookie = Cookie::get('basket');
        if ($cookie)
            return response()->json([
                // 'redirect_url' => route('cabinet'),
                'chat_id' => $chat['id'] ,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ])->withCookie(Cookie::forget('basket'));

        return response()->json([
            // 'redirect_url' => route('cabinet'),
            'chat_id' => $chat['id'] ,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function update(Request $request)
    {
        $user = $request->user();
        foreach ($request as $key=>$item){
            $user[$key] = $item;
        }
        $user->save();
        return response()->json(['msg'=> 'Successfully updated']);


    }
}
