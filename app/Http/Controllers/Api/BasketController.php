<?php

namespace App\Http\Controllers\Api;

use App\Dimention;
use App\Http\Controllers\Controller;

use App\Product;
use App\ProductVariation;
use App\ProductVariationAttribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class BasketController extends Controller
{
    //
    public function show()
    {

        $basket = Cookie::get('basket');
        if (!$basket)
            return response()->json(['msg' => 'Cart is empty'], 404);
        $basket = explode(',', $basket);
        $basketNew = [];
        unset($basket[count($basket) - 1]);
        foreach ($basket as $item) {
            $item = explode('_', $item);

            $item['product'] = Product::find($item[0]);

            $item['product']['variations'] = ProductVariation::where('product_id', $item[0])->join('product_variation_attributes', 'product_variations.id', '=', 'product_variation_attributes.product_variation_id')
                ->select('product_variations.id', 'product_variation_attributes.*', 'product_variations.*')
                ->get();

            foreach ($item['product']['variations'] as $variation) {
                $variation['discount']  = $variation['discount'] != null ? $variation['discount'] : 0;
                $variation['selected'] = 0;
                $variation['dimension_name'] = Dimention::find($variation['dimention_id'])->pluck('name')->first();
                if ($variation['id'] == $item[2]) {
                    $variation['selected'] = 1;
                }
                $variation['cashback'] = setting('site.percent').'%';
                // $variation['dimension'] = Dimention::where('id',$item[3])->pluck('name')->first();
            }
            $item['product']['quantity'] = $item[1];
            for ($i = 0; $i <= 2; $i++) {
                unset($item[$i]);
            }

            $item = $item['product'];
            $basketNew[] = $item;
        }

        $basket = $basketNew;
        foreach ($basket as $item) {

            $item['thumbnail'] = asset('/storage/' . $item['thumbnail']);
        }


        return response()->json(['basket' => $basket], 200);
    }

    public function mobile(Request $request)
    {

        $validated = $request->validate([
            'cart' => 'required',

        ]);
        $cart = [];
        foreach ($validated['cart'] as $item) {
            $item['product'] = Product::find($item['product']);
            $item['variations'] = ProductVariation::where('product_id', $item['product']['id'])
                ->join('product_variation_attributes', 'product_variations.id', '=', 'product_variation_attributes.product_variation_id')
                ->get();
            foreach ($item['variations'] as $variation) {
                if ($variation['id'] == $item['selected_variation']) {
                    $variation['select'] = 1;
                    $variation['cashback'] = setting('site.percent').'%';
                } else {
                    $variation['select'] = 0;
                    $variation['cashback'] = setting('site.percent').'%';

                }
            }
            $cart[] = $item;
        }
        foreach ($cart as $item) {
            $item['product']['thumbnail'] = asset('/storage/' . $item['product']['thumbnail']);
            $item['product']['slider_images'] = json_decode($item['product']['slider_images']);
            $images = [];

            if ($request->lang){
                $item['product']['name'] = $item['product']->getTranslatedAttribute('name',$request->lang);
                $item['product']['description'] = $item['product']->getTranslatedAttribute('description',$request->lang);
            }

            if ($item['product']['slider_images'] != [])
                foreach ($item['product']['slider_images']  as $image) {
                    $images[] = asset('/storage/' . $image);
                }
            $item['product']['slider_images'] = $images;
        }
        return response()->json(['cart' => $cart], 200);
    }
    public function add(Request $request)
    {

        $user = \auth('api')->user();
        if(!$user){
            return response()->json(['msg'=>'Пожалуйста авторизуйтесь','url'=>route('auth_required')],400);
        }
        if ($request->product_id && $request->variation) {
            $product = $request->product_id;
            $variation = $request->variation;
            $product_item = Product::find($product);
            $variation_item = ProductVariation::find($variation);
            if (!$product_item || !$variation_item)
                return response()->json(['msg' => 'Not found'], 404);


            $basket = Cookie::get('basket');

            if (!$basket) {
                $basket = $product_item['id'] . '_1_' . $variation . ',';
            } else {
                $basket = explode(',', $basket);


                $newString = '';
                $i = 0;
                $exception = 0;
                foreach ($basket as $item) {

                    $item = explode('_', $item);
                    if (intval($item[0]) == $request->product_id && intval($item[2]) == $request->variation) {




                        $item[1] += 1;
                        strval($item[1]);

                        $item = implode('_', $item);

                        $basket[$i] = $item;
                        $exception = 1;
                        break;
                    }
                    $i += 1;
                }
                $basket = implode(',', $basket);

                if ($exception == 0) {

                    $newString = $product . '_1_' . $variation  . ',';

                    $basket .= $newString;
                }
            }

            $cookie = \cookie('basket', $basket, 1000000);
            $counter = session()->get('basket_counter');
            if (!$counter){
                session()->put('basket_counter',1);
            }else{
                $counter += 1;
                session()->put('basket_counter',$counter);
            }
            session()->save();
            $local = Cookie::get('local');

            if ($local == 'en')
                return response()->json(['msg' => 'Product added'], 200)->withCookie($cookie);

            return response()->json(['msg' => 'Перейти в корзину'], 200)->withCookie($cookie);
        }

        return response()->json(['msg' => 'Error'], 400);
    }

    public function delete(Request $request)
    {
        if ($request->product_id && $request->variation) {
            $basket = Cookie::get('basket');

            if (!$basket)
                return response()->json(['msg' => 'Cart not found'], 404);
            $exception = 0;

            $basket = explode(',', $basket);
            $i = 0;
            foreach ($basket as $item) {
                $item = explode('_', $item);
                if (intval($item[0]) == $request->product_id && intval($item[2]) == $request->variation) {
                    $item[1] -= 1;
                    if ($item[1] != 0) {
                        strval($item[1]);

                        $item = implode('_', $item);

                        $basket[$i] = $item;
                    } else {

                        unset($basket[$i]);
                    }

                    $exception = 1;
                    break;
                }
                $i += 1;
            }

            if ($exception == 0) {
                return response()->json(['msg' => 'Product not found in cart'], 404);
            }
            $basket = implode(',', $basket);

            $counter = session()->get('basket_counter');
            if (!$counter){
                session()->put('basket_counter',0);
            }else{
                $counter -= 1;
                session()->put('basket_counter',$counter);
            }
            session()->save();
            $cookie = \cookie('basket', $basket, 1000000);
            return response()->json(['msg' => 'Product deleted'], 200)->withCookie($cookie);
        }

        return response()->json(['msg' => 'error'], 400);
    }
    public function increase(Request $request)
    {
        if ($request->product_id && $request->variation) {
            $basket = Cookie::get('basket');

            if (!$basket)
                return response()->json(['msg' => 'Cart not found'], 404);
            $exception = 0;

            $basket = explode(',', $basket);
            $i = 0;
            foreach ($basket as $item) {
                $item = explode('_', $item);
                if (intval($item[0]) == $request->product_id && intval($item[2]) == $request->variation) {
                    $item[1] += 1;
                    if ($item[1] != 0) {
                        strval($item[1]);

                        $item = implode('_', $item);

                        $basket[$i] = $item;
                    } else {

                        unset($basket[$i]);
                    }

                    $exception = 1;
                    break;
                }
                $i += 1;
            }

            if ($exception == 0) {
                return response()->json(['msg' => 'Product not found in cart'], 404);
            }
            $basket = implode(',', $basket);
            $counter = session()->get('basket_counter');
            if (!$counter){
                session()->put('basket_counter',1);
            }else{
                $counter += 1;
                session()->put('basket_counter',$counter);
            }
            session()->save();
            $cookie = \cookie('basket', $basket, 1000000);
            return response()->json(['msg' => 'Product deleted'], 200)->withCookie($cookie);
        }
        return response()->json(['msg' => 'error'], 400);
    }

    public function deleteAll(Request $request)
    {
        if ($request->product_id && $request->variation) {
            $basket = Cookie::get('basket');

            if (!$basket)
                return response()->json(['msg' => 'Cart not found'], 404);


            $basket = explode(',', $basket);
            $i = 0;
            foreach ($basket as $item) {
                $item = explode('_', $item);
                if (intval($item[0]) == $request->product_id && intval($item[2]) == $request->variation) {

                    unset($basket[$i]);
                    $counter = session()->get('basket_counter');
                    if (!$counter){
                        session()->put('basket_counter',1);
                    }else{
                        $counter -= $item[1];
                        session()->put('basket_counter',$counter);
                    }
                    session()->save();


                    $exception = 1;
                    break;
                }
                $i += 1;
            }

            if ($exception == 0) {
                return response()->json(['msg' => 'Product not found in cart'], 404);
            }
            $basket = implode(',', $basket);

            $cookie = \cookie('basket', $basket, 1000000);
            return response()->json(['msg' => 'Product deleted'], 200)->withCookie($cookie);
        }
        return response()->json(['msg' => 'error'], 400);
    }
    public function clear(Request $request)
    {
        $basket = Cookie::get('basket');

        if (!$basket)
            return response()->json(['msg' => 'error'], 400);

        session()->put('basket_counter',0);
        session()->save();

        return response()->json(['msg' => 'Cleared'], 200)->withCookie(Cookie::forget('basket'));
    }


}
