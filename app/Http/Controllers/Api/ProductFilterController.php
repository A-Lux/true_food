<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\ProductVariation;
use Illuminate\Http\Request;
use App\ProductVariationAttribute;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Filter\ProductVariationAttributeFilter;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductFilterController extends Controller
{
    public function filter(Request $request, ProductVariationAttributeFilter $filter)
    {
        $this->validate($request, [
            'paginated' => 'nullable|boolean',
            'popular' => 'nullable|boolean',
            'dishOfTheWeek' => 'nullable|boolean'
        ]);

        $variationIds = ProductVariationAttribute::filter($filter)
            ->get()
            ->pluck('product_variation_id');
        $price = $request->price;
        $byPrice = $request->byPrice;
        $products = Product::where('is_show',1)->with(['variations' => Product::loadVariations($withAttributes = true), 'category'])
            ->whereHas('variations', function ($query) use ($variationIds, $price, $byPrice) {
                $query->orderBy('price','asc');

                if ($byPrice)
                    $query->orderBy('price',$byPrice);



                if ($price)
                    $query->whereBetween('price',$price);

                $query->whereIn('id', $variationIds);
            });






        if ($request->category) {
            $products = $products->whereIn('category_id',$request->category);
        }

        if ($request->popular) {
            $products = $products->popular();
        }

        if ($request->dishOfTheWeek) {
            $products = $products->dishOfTheWeek();
        }







        if ($request->paginated == true) {

            $products = $products->paginate($request->amount ?: 15);
        } else {
            $products = $products->limit($request->amount ?: 15)->get();
        }
        if ($byPrice == 'desc') {
            $products = $products->sortByDesc(function ($item) {
                return $item->variations->max('price');
            })->values();
            if ($request->paginated)
                $products = $this->paginate($products,$request->amount ?: 15);
        }

        if ($byPrice == 'asc')
        {
            $products = $products->sortBy(function ($item) {
                return $item->variations->max('price');
            })->values();
            if ($request->paginated)
                $products = $this->paginate($products,$request->amount ?: 15);

        }


        return new ProductCollection($products, $request);
    }
    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
