<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutusController extends Controller
{
    //

    public function index(Request $request)
    {
        $data['image'] = setting('o-nas.image');
        if($request->local == 'en'){
            $data['text'] = setting('o-nas.abouten');
        }
        $data['text'] = strip_tags(setting('o-nas.about'));


        return response()->json([$data],200);
    }

    public function contacts(Request $request)
    {
        $data['phone'] = setting('kontakty.phone');
        $data['email'] = setting('kontakty.email');
        $data['address'] = setting('kontakty.addess');
        if ($request->local == 'en')
            $data['address'] = setting('kontakty.en-address');

        return response()->json(['data'=>$data],200);
    }
}
