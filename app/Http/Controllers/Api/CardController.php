<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\UserCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CardController extends Controller
{
    //

    public function addCard(Request $request)
    {
        $user = $request->user();
        $query = [
            'pg_merchant_id'=> 532183,
            'pg_user_id'=> $user->id,
            'pg_salt' => Str::random(15),
            'pg_back_link' => route('login'),
            'pg_post_link' => route('saveCard'),
        ];

         //sort alphabetically
        array_unshift($query, 'add');

        array_push($query, '6wfqi40siz3Pw4K8'); //add your secret key (you can take it in your personal cabinet on paybox system)

        ksort($query);
        $query['pg_sig'] = md5(implode(';', $query));
        unset($query[0], $query[1]);


        $response = Http::withHeaders([
            'X-Content-Type'=> 'multipart/form-data'

        ])->post('https://api.paybox.money/v1/merchant/532183/cardstorage/add',$query);

        $xml = new \SimpleXMLElement($response->body());

        $data = [];
        foreach ($xml as $value) {

            $data[] = (string)$value;
        }

        return response()->json(['payment_url'=> $data[3]],200);
    }

    public function saveCard(Request $request)
    {
        $xml = $request->pg_xml;


        $data = new \SimpleXMLElement($xml);
        foreach ($data as $key=>$value) {

            $data[$key] = str_replace(" ",'',$data[$key]);
        }
        try{


            $newCard = UserCard::create([
                'user_id' => $data->pg_user_id,
                'card_3ds' => $data->pg_card_3ds,
                'card_hash' => $data->pg_card_hash,
                'card_id' => $data->pg_card_id,
                'card_month' => $data->pg_card_month,
                'card_year' => $data->pg_card_year,
            ]);

            return response([],200);
        }catch (\Exception $e){
            Log::error('Error '.$e);
            Log::error('Error '.print_r($data,true));
            return response(['error' => 'Error '.print_r($data,true)],500);

        }
    }

    public function getCards(Request $request)
    {
        $user = $request->user();
        $cards = UserCard::where('user_id',$user['id'])->get();
        return response(['cards' => $cards],200);
    }

    public function deleteCard($id, Request $request)
    {
        $user_card = UserCard::find($id);
        if (!$user_card){
            if ($request->has('local') && $request->local == 'en' ){
                return response()->json(['message' => 'Card not found'],200);
            }else{
                return response()->json(['message' => 'Карта не найдена'],200);
            }
        }
        $user_card->delete();
        if ($request->has('local') && $request->local == 'en' ){
            return response()->json(['message' => 'Card Deleted'],200);
        }else{
            return response()->json(['message' => 'Карта удалена'],200);
        }
    }



}
