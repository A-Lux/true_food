<?php

namespace App\Http\Controllers\Api;

use App\DeliveryType;
use App\OrderType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class OrderTypeController extends Controller
{
    //
    public function show()
    {
        $order_types = DeliveryType::get();
        $local = Cookie::get('local');
        $order_types = $order_types->translate($order_types,$local);
        return response(['order_types' => $order_types],200);
    }
}
