<?php

namespace App\Http\Controllers\Api;

use App\Feedback;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    //
    public function show(Request  $request)
    {
        if (!$request->product_id)
            return response()->json(['msg' => 'Not found'], 404);
        $reviews = Feedback::where('product_id', $request->product_id)->get();

        return  response()->json(['reviews' => $reviews], 200);
    }

    public function store(Request  $request)
    {
        $validated = $request->validate([
            'product_id' => 'required',
            'message' => 'required',
            'rating' => 'required'
        ]);

        $feedback = Feedback::create([
            'product_id' => $request->has('product_id') ? $request->product_id : null,
            'user_id' => null,
            'message' => $request->message,
            'rating' => $request->rating,
            'attachment' => $request->has('attachment') ? $request->attachment : null,
            'email' => $request->has('email') ? $request->email : null
        ]);


        $product = Product::find($feedback['product_id']);
        $product['rating'] += $feedback['rating'];
        $product['review_count'] += 1;
        $product->save();

        return response()->json(['msg' => 'Review added','feedback' => $feedback], 200);
    }
}
