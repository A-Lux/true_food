<?php

namespace App\Http\Controllers\Front;

use App\Events\MessageSent;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CatalogController extends Controller
{
    //
    public function Index($slug = null)
    {
        $local = session()->get('lang');
        App::setLocale($local);
        $categoryId = null;
        if ($slug!=null)
            $categoryId = Category::where('slug',$slug)->first();

        return view('products.index',compact('categoryId'));
    }


    public function test(Request $request)
    {
        $user = $request->user;
        $message = $request->message;

        event(new MessageSent($user,$message));
        return 'hi';
    }
}
