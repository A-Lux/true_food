<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Category;
use App\Location;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\ProductVariation;
use App\ProductVariationAttribute;
use App\SliderImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Cookie;

class IndexController extends Controller
{
    //
    public function Index(Request $request)
    {

        $location = \Stevebauman\Location\Facades\Location::get($request->ip());
        Log::error('Location : '.print_r((array) $location,true));
        $local = session()->get('lang');
        App::setLocale($local);
        if (!$local){
            $local = 'ru';
            session()->put('lang',$local);
            session()->save();
            App::setLocale('ru');
        }

        \Illuminate\Support\Facades\Cookie::queue('local',$local , 525600);
        $data['dishCategories'] = Category::where('is_dish',1)->orderBy('order','asc')->get();
        $data['drinkCategories'] = Category::where('is_dish',0)->orderBy('order','asc')->get();
        $data['dayProducts'] = Product::where('is_new',1)->where('is_show',1)->get();
//        $data['dishCategories']->translate($local);
//        dd($data['dishCategories'][count($data['dishCategories'])-1]);
//

        $data['popularProducts'] = Product::where('is_popular',1)->where('is_show',1)->get();
        $data['sliders'] = SliderImage::orderBy('id','asc')->get();
        $data['sliders'] = $data['sliders']->translate($data['sliders'],$local);
        $variations = ProductVariation::get();
        $local = session()->get('lang');
        $data['drinkCategories'] = $data['drinkCategories']->translate($data['drinkCategories'],$local);
        $data['dishCategories'] = $data['dishCategories']->translate($data['dishCategories'],$local);


        return view('home.index',$data);
    }
    public function Auth()
    {

        $local = session()->get('lang');
        App::setLocale($local);
        if (!$local){
            $local = 'ru';
            session()->put('lang',$local);
            session()->save();
            App::setLocale('ru');
        }

        \Illuminate\Support\Facades\Cookie::queue('local',$local , 525600);
        $data['dishCategories'] = Category::where('is_dish',1)->orderBy('order','asc')->get();
        $data['drinkCategories'] = Category::where('is_dish',0)->orderBy('order','asc')->get();
        $data['dayProducts'] = Product::where('is_new',1)->where('is_show',1)->get();
//        $data['dishCategories']->translate($local);
//        dd($data['dishCategories'][count($data['dishCategories'])-1]);
//

        $data['popularProducts'] = Product::where('is_popular',1)->where('is_show',1)->get();
        $data['sliders'] = SliderImage::orderBy('id','asc')->get();
        $data['sliders'] = $data['sliders']->translate($data['sliders'],$local);
        $variations = ProductVariation::get();
        $local = session()->get('lang');
        $data['drinkCategories'] = $data['drinkCategories']->translate($data['drinkCategories'],$local);
        $data['dishCategories'] = $data['dishCategories']->translate($data['dishCategories'],$local);

        $data['from_payment'] = 1;
        return view('home.index',$data);
    }

    public function switch($lang)
    {

        App::setLocale($lang);

        $local = session()->put('lang',$lang);
        session()->save();

        \Illuminate\Support\Facades\Cookie::queue('local', $lang, 525600);
        return redirect()->back();
    }
}
