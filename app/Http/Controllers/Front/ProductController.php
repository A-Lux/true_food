<?php

namespace App\Http\Controllers\Front;

use App\Category;
use App\Dimention;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductVariation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    //
    public function Show($slug)
    {
        $local = session()->get('lang');
        App::setLocale($local);
        $data['popularProducts'] = Product::get();
        $data['productMain'] = Product::withTranslation('en')->where('slug', $slug)->first();
        $data['productMain']['name'] = $data['productMain']->getTranslatedAttribute('name',$data['productMain'],$local);
        $data['productMain']['description'] = $data['productMain']->getTranslatedAttribute('description',$data['productMain'],$local);

        $data['productMain']['variations'] = ProductVariation::where('product_id', $data['productMain']['id'])
            ->join('product_variation_attributes', 'product_variations.id', '=', 'product_variation_id')
            ->select('product_variations.id', 'product_id', 'sku', 'price', 'discount', 'product_variation_id', 'attribute_id', 'value', 'dimention_id', 'is_main', 'cashback')
            ->get();
        $data['variations'] = ProductVariation::get();

        if (count($data['productMain']['variations']))
            $data['dimension'] = Dimention::find($data['productMain']['variations'][0]['dimention_id'])->pluck('name')->first();

        $data['productMain']['rating'] = intval($data['productMain']['rating']);
        if ($data['productMain']['review_count'])
            $data['productMain']['rating'] = intval($data['productMain']['rating'] / $data['productMain']['review_count']);

        $data['productMain']['slider_images'] = json_decode($data['productMain']['slider_images']);
        $data['category'] = Category::where('id', $data['productMain']['category_id'])->first();
        $data['category'] = $data['category']->translate($data['category'],$local);
        $data['category'] = $data['category']['name'];
        if (!isset($data['dimension']))
            $data['dimension'] = 'OK';

        return view('products.show', $data);
    }

    public function Success()
    {
        $local = session()->get('lang');
        App::setLocale($local);
        $data['popularProducts'] = Product::where('is_popular', 1)->get();
        return view('cart.index', $data)->with('message', 'Заказ оплачен');
    }
    public function Fail()
    {
        $local = session()->get('lang');
        App::setLocale($local);
        $data['popularProducts'] = Product::where('is_popular', 1)->get();
        return view('cart.index', $data)->withErrors('Ошибка с заказом');
    }

    public function RequestPayment(Request $request)
    {

        $query = [
            'pg_merchant_id' => 532183,
            'pg_amount' => $request->totalPrice,
            'pg_salt' => Str::random(15),
            'pg_order_id' => $request->order_id,
            'pg_description' => $request->description,
            'pg_result_url' => route('orderStatus'),
            'pg_success_url' => route('orderSuccess'),
            'pg_success_url_method' => 'AUTOGET',

        ];

        $query['pg_testing_mode'] = 1; //add this parameter to request for testing payments

        //if you pass any of your parameters, which you want to get back after the payment, then add them. For example:
        // $request['client_name'] = 'My Name';
        // $request['client_address'] = 'Earth Planet';

        //generate a signature and add it to the array
        ksort($query); //sort alphabetically
        array_unshift($query, 'payment.php');
        array_push($query, '6wfqi40siz3Pw4K8'); //add your secret key (you can take it in your personal cabinet on paybox system)


        $query['pg_sig'] = md5(implode(';', $query));

        unset($query[0], $query[1]);

        $query = http_build_query($query);
        header('Location:https://api.paybox.money/payment.php?' . $query);
        exit;
    }
}
