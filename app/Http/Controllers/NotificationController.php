<?php

namespace App\Http\Controllers;

use App\Chat;
use App\PushNotification;
use App\User;
use App\UserDeviceToken;
use App\UserMessage;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    //
    protected $serverKey = 'AAAAdc7Xf4g:APA91bH_PvWK5vt39Q8jBEIKWCAUZ2I9q_H_r3C2fQQK-15A6vy9a-RAE_5Lkfl4sjrtqojO2Vg2BAYjo1x2a_HPRlOwNYBm-62N28SckxDuqYIij_SWs1gpynhjqE9-OUzoNW3GKTzP';

    public function show()
    {
        $data['notifications'] = PushNotification::orderBy('id','desc')->paginate(15);
        return view('vendor.voyager.notification.index',$data);
    }

    public function store(Request $request)
    {

        $admin = User::find(1);
        $user_device_tokens = UserDeviceToken::get();
        $push = new PushNotification();
        $push['message'] = $request->message;
        $push->save();
        $users = User::where('id','!=',1)->get();
        foreach ($users as $user){
            $chat = Chat::where('user_id',$user->id)->first();
            if (!$chat){
                $chat = Chat::create((['user_id' => $user->id,'admin_id'=>$admin->id]));
                $userMessage = UserMessage::create([
                    'user_id'=>$admin->id,
                    'message'=>$request->message,
                    'title'=>0,
                    'chat_id' => $chat['id'],
                    'is_read' =>0]);
            }
            if ($chat){
                $userMessage = UserMessage::create([
                    'user_id'=>$admin->id,
                    'message'=>$request->message,
                    'title'=>0,
                    'chat_id' => $chat['id'],
                    'is_read' =>0]);
            }
        }

        foreach ($user_device_tokens as $device) {
                $user = User::find($device->user_id);



                $data = [
                    "to" => $device->device_token,
                    "notification" =>
                        [
                            "title" => $admin->name,
                            "body" => $request->message,
                            "icon" => asset('/images/logo.svg'),
                            "badge" => 1

                        ],
                ];
                $dataString = json_encode($data);

                $headers = [
                    'Authorization: key=' . $this->serverKey,
                    'Content-Type: application/json',
                ];

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

                curl_exec($ch);
            }
        return back()->with('message','Отправлено');
    }


}
