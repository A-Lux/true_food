<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;

class PickupController extends Controller
{
    //
    public function show()
    {
        $locations = Location::get();

        return response(['places' => $locations], 200);
    }
}
