<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Events\MessageSent;
use App\User;
use App\UserMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Psy\Util\Str;
use Ramsey\Collection\Collection;

class ChatController extends Controller
{
    protected $serverKey = 'AAAAdc7Xf4g:APA91bH_PvWK5vt39Q8jBEIKWCAUZ2I9q_H_r3C2fQQK-15A6vy9a-RAE_5Lkfl4sjrtqojO2Vg2BAYjo1x2a_HPRlOwNYBm-62N28SckxDuqYIij_SWs1gpynhjqE9-OUzoNW3GKTzP';

    //
    public function index()
    {
        $data['chats'] = Chat::whereHas('messages')->get();
        foreach ($data['chats'] as $chat){
            $chat['firstMessage'] = UserMessage::where('chat_id',$chat['id'])
                ->join('users','users.id','=','user_messages.user_id')
                ->orderBy('user_messages.id','desc')
                ->select('users.name','user_messages.*')
                ->first();
            $chat['firstMessage']['created_at'] = Carbon::createFromFormat('Y-m-d H:i:s',$chat['firstMessage']['created_at'],'UTC');
            $chat['firstMessage']['created_at']->tz = 'Etc/GMT-6';
            $chat['user_message_order'] = $chat['firstMessage']['id'];
        }
        $data['chats'] = $data['chats']->sortByDesc('user_message_order');
        $data['chats'] = (new \App\Support\Collection($data['chats']))->paginate(20);

        return view('vendor.voyager.chat.chat',$data);
    }

    public function show($chat)
    {
        $chat = Chat::find($chat);
        $messages = UserMessage::where('chat_id',$chat->id)->orderBy('id','asc')->get();
        foreach ($messages as $message){
            $message['is_read'] = 1;
            $message->save();
        }
        return view('vendor.voyager.chat.show',['chat' => $chat,'messages'=>$messages]);
    }


    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'message' => 'required',

        ]);

        if ($request->has('chat_id')){
            $admin = User::where('role_id',1)->first();
            $chat = Chat::find($request->chat_id);
            $message = UserMessage::create([
                'user_id' => $admin->id,
                'message' => $request->message,
                'title' => $request->has('image') ? $request->file('image')->store('image'):0,
                'is_read' => 0,
                'chat_id' => $chat->id
            ]);
            $user = User::find($request->user_id);

            if (count($user->device_token)>0) {

                foreach ($user->device_token as $device) {
                    $data = [
                        "to" => $device->device_token,
                        "notification" =>
                            [
                                "title" => $admin->name,
                                "body" => $request->message,
                                "icon" => asset('/images/logo.svg')
                            ],
                    ];
                    $dataString = json_encode($data);

                    $headers = [
                        'Authorization: key=' . $this->serverKey,
                        'Content-Type: application/json',
                    ];

                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

                    curl_exec($ch);
                }
            }
            event(new MessageSent($message,$chat->id));
            return redirect()->back();
        }else{
            $admins = User::where('role_id',1)->count();
            $chat = Chat::create([
               'user_id' => $request->has('user_id') ? $request->user_id : null,
               'admin_id' =>1,
            ]);
            $message = UserMessage::create([
                'user_id' => $request->has('user_id') ? $request->user_id : null,
                'message' => $request->message,
                'title' => $request->has('image') ? $request->file('image')->store('image'): 0,
                'is_read' => 0,
                'chat_id' => $chat->id
            ]);
            $user = User::find($message->user_id);
            if (count($user->device_token)>0)
                foreach ($user->device_token as $device){
                    $data = [
                        "to" => $device->device_token,
                        "notification" =>
                            [
                                "title" => $user->name,
                                "body" => $message,
                                "icon" => asset('/images/logo.svg'),
                                "badge" => 1
                            ],
                    ];
                    $dataString = json_encode($data);

                    $headers = [
                        'Authorization: key=' . $this->serverKey,
                        'Content-Type: application/json',
                    ];

                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

                    curl_exec($ch);
                }
            }

            event(new MessageSent($message,$chat->id));
            return  redirect()->back();
        }

}
