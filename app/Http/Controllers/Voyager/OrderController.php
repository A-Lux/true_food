<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use App\Product;

use App\Status;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    protected $serverKey = 'AAAAdc7Xf4g:APA91bH_PvWK5vt39Q8jBEIKWCAUZ2I9q_H_r3C2fQQK-15A6vy9a-RAE_5Lkfl4sjrtqojO2Vg2BAYjo1x2a_HPRlOwNYBm-62N28SckxDuqYIij_SWs1gpynhjqE9-OUzoNW3GKTzP';

    public function orders(Request $request)
    {

        $data['orders'] = Order::where(function($query){
            $query->where('payment_type_id','!=',1);
        })->orWhere(function($query){
          $query->where('status_id','!=',1);
          $query->where('payment_type_id',1);
        })->where('order_type_id','!=',2)->orderBy('id','desc')->paginate(15);
        if($request->has('order_id') && $request->order_id != ''){

            $data['orders'] = Order::where(function($query) use ($request){
                $query->where('id',$request->order_id);
                $query->where('payment_type_id','!=',1);
            })->orWhere(function($query) use ($request){
                $query->where('id',$request->order_id);
                $query->where('status_id',6);
                $query->where('payment_type_id',1);
            })->where('order_type_id','!=',2)->orderBy('id','desc')->paginate(15);

        }

        foreach($data['orders'] as $order){
            $details = OrderDetail::where('order_id',$order->id)->get();
            $order['sum'] = 0;
            foreach ($details as $detail){
                $order['sum'] += $detail['unit_price'];
            }

            $order['sum']-=$order['used_points'];
        }
        return view('vendor.voyager.orders.show',$data);
    }

    public function show($id)
    {
        $data['order'] = Order::find($id);
        $data['order_details'] = OrderDetail::where('order_id',$id)->get();
        $data['statuses'] = Status::get();
        return view('vendor.voyager.orders.single',$data);
    }
    public function updateStatus(Request $request){
        $valData = $request->validate([
            'order_id' => 'required',
            'status_id' => 'required'
        ]);

        $order = Order::find($valData['order_id']);
        if ($valData['status_id'] == $order['id'])
            return back();


        if ($valData['status_id'] == 5 && $valData['status_id'] != $order['status_id'] && $order['payment_type_id'] != 1) {
            $order_details = OrderDetail::where('id', $order['id'])->get();
            $order['status_id'] = $valData['status_id'];
            $main_cash = 0;
            $main_price = 0;
            foreach ($order_details as $order_detail) {
                $main_price += $order_detail->unit_price;
                $main_cash += $order_detail->unit_cashback;
            }

            if ($order['user_id']) {
                $user = User::find($order['user_id']);
                $user['bill'] += (setting('site.percent')/100)*($main_price-$order['used_points']);

                $user->save();
                $admin = User::find(1);
                $status = Status::find($valData['status_id']);
                if (count($user->device_token) > 0) {

                    foreach ($user->device_token as $device) {
                        $data = [
                            "to" => $device->device_token,
                            "notification" =>
                                [
                                    "title" => $admin->name,
                                    "body" => "Статус вашего заказа номер ".$order['id']. " : " . $status->name,
                                    "icon" => asset('/images/logo.svg'),
                                    "badge" => 1
                                ],
                        ];
                        $dataString = json_encode($data);

                        $headers = [
                            'Authorization: key=' . $this->serverKey,
                            'Content-Type: application/json',
                        ];

                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

                        curl_exec($ch);
                    }

                }
            }


                $order->save();
                return back()->with('message', 'Статус обновлен');
            } else {
                $order['status_id'] = $valData['status_id'];
                $order->save();
                if ($order['user_id']) {


                    $user = User::find($order['user_id']);
                    $admin = User::find(1);
                    $status = Status::find($valData['status_id']);
                    if (count($user->device_token) > 0) {

                        foreach ($user->device_token as $device) {
                            $data = [
                                "to" => $device->device_token,
                                "notification" =>
                                    [
                                        "title" => $admin->name,
                                        "body" => "Статус вашего заказа номер ".$order['id']. " : " . $status->name,
                                        "icon" => asset('/images/logo.svg')
                                    ],
                            ];
                            $dataString = json_encode($data);

                            $headers = [
                                'Authorization: key=' . $this->serverKey,
                                'Content-Type: application/json',
                            ];

                            $ch = curl_init();

                            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

                            curl_exec($ch);
                        }

                    }

                         return back()->with('message', 'Статус обновлен');
                    }
            }


    }

    public function prove($id)
    {
        $data['order'] = Order::find($id);

        if ($data['order']['status_id'] == 6){
            $data['order']['status_id'] = 5;
            $data['order']->save();
            return back()->with('message','Изменено');

        }

        $data['order']['status_id'] = 5;
        $data['order']->save();

        $order_details = OrderDetail::where('order_id',$data['order']['id'])->get();
        if ($data['order']['payment_type_id'] == 1)
            return back()->with('message','Изменено');

        $main_cash = 0;

        foreach ($order_details as $order_detail){

            $main_cash += $order_detail->unit_price;

        }


        if ($data['order']['user_id']){
            $user = User::find($data['order']['user_id']);
            $user['bill']+= (setting('site.percent')/100)*($main_cash-$data['order']['used_points']);

            $user->save();
        }
        return back()->with('message','Изменено');
    }

}
