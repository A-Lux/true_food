<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use App\ProductVariationAttribute;
use App\Http\Controllers\Controller;

class AttributeController extends Controller
{
    public function store(Request $request, $productVariation)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer',
            'value' => 'required',
            'dimention_id' => 'required'
        ]);
        
        $attribute = ProductVariationAttribute::create([
            'product_variation_id' => $productVariation,
            'attribute_id' => $validatedData['id'],
            'value' => $validatedData['value'],
            'dimention_id' => $validatedData['dimention_id']
        ]);

        return response($attribute, 201);
    }

    public function update(Request $request, $productVariation, ProductVariationAttribute $productVariationAttribute)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer',
            'value' => 'required',
            'dimention_id' => 'required'
        ]);

        $productVariationAttribute->fill([
            'product_variation_id' => $productVariation,
            'attribute_id' => $validatedData['id'],
            'value' => $validatedData['value'],
            'dimention_id' => $validatedData['dimention_id']
        ]);

        $productVariationAttribute->save();

        return response($productVariationAttribute, 200);
    }

    public function destroy(ProductVariationAttribute $productVariationAttribute)
    {
        $productVariationAttribute->delete();

        return response()->noContent();
    }
}
