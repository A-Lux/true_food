<?php

namespace App\Http\Controllers\Voyager;

use App\ProductVariation;
use App\ProductVariationAttribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VariationController extends Controller
{
    public function store(Request $request, $product)
    {

            $validatedData = $request->validate([
                "cashback" => "required|integer",
                "discount" => "required|integer",
                "price" => "required|integer",
                "sku" => "required|string|unique:product_variations"
            ]);

            $validatedData['cashback'] = 0;
            $validatedData['product_id'] = $product;

            $variation = ProductVariation::create($validatedData);
            $attributeData = [
                'product_variation_id' => $variation->id,
                'attribute_id' => 1,
                'dimention_id' => 1,
                'is_main' => 1,
            ];
            $attribute = ProductVariationAttribute::create($attributeData);
            $variation->load(['attributes' => ProductVariation::loadAttributeWithDimention(), 'mainAttributes' => ProductVariation::loadAttributeWithDimention()]);
            return response($variation, 201);

    }

    public function update(Request $request, $product, ProductVariation $productVariation)
    {
        $validatedData = $request->validate([
            "discount" => "required|integer",
            "price" => "required|integer",
            "sku" => "required|string"
        ]);
        $validatedData["cashback"] = 0;
        $productVariation->fill($validatedData);
        $productVariation->save();

        $attributes = ProductVariationAttribute::where('product_variation_id',$productVariation->id)->get();
        if (count($attributes) == 0){
            $attributeData = [
                'product_variation_id' => $productVariation->id,
                'attribute_id' => 1,
                'dimention_id' => 1,
                'is_main' => 1,
            ];
            $attribute = ProductVariationAttribute::create($attributeData);
        }
        $productVariation->load(['attributes' => ProductVariation::loadAttributeWithDimention(), 'mainAttributes' => ProductVariation::loadAttributeWithDimention()]);
        return response($productVariation, 200);
    }

    public function destroy(ProductVariation $productVariation)
    {
        $productVariation->delete();

        return response()->noContent();
    }
}
