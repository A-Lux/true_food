<?php

namespace App\Http\Controllers;

use App\OrderType;
use Illuminate\Http\Request;

class OrderTypeController extends Controller
{
    //
    public function show()
    {
        $order_types = OrderType::get();
        return response(['order_types' => $order_types],200);
    }
}
