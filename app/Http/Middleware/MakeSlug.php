<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class MakeSlug
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!is_null($request->slug)) {
            if($request->method() == 'POST' || $request->method() == 'PUT') {
                $slug = Str::slug($request->slug);
                $request->merge(['slug' => $slug]);
            }
        }
        
        return $next($request);
    }
}
