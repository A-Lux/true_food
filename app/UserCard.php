<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    //
    protected $fillable = [
        'user_id',
        'card_3ds',
        'card_hash',
        'card_id',
        'card_month',
        'card_year'
    ];
}
