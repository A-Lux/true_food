<?php

namespace App;

use App\Status;
use App\GiftCard;
use App\OrderDetail;
use App\ProductVariation;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'order_type_id',
        'status_id',
        'email',
        'payment_type_id',
        'comment',
        'phone',
        'location_id',
        'cabinet_number',
        'pick_up_at',
        'delivery_type_id',
        'delivery_place_id',
        'used_points'
    ];
    protected $translatable;
    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class)->select('id', 'name');
    }

    public function place(){
        return $this->belongsTo(DeliveryPlace::class,'delivery_place_id','id')->select('name');
    }

    public function orderType(){
        return $this->belongsTo(OrderType::class)->select('name');
    }

    public function paymentType()
    {

        return $this->belongsTo(PaymentType::class)->select('name');
    }
    public function user(){
        return $this->belongsTo(User::class)->select('name');
    }

    public function deliveryType()
    {
        return $this->belongsTo(DeliveryType::class)->select('name');
    }
}
