<?php

namespace App;

use App\Category;
use App\Traits\HasVariations;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Product extends Model
{
    use HasVariations;
    use Translatable;

    protected $translatable = [
        'name',
        'description',
    ];
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function variations(){
        return $this->hasMany('App\ProductVariation')->orderBy('price','asc');
    }

    public function scopePopular($query)
    {
        return $query->where('is_popular', 1);
    }

    public function scopeDishOfTheWeek($query)
    {
        return $query->where('is_dish_of_the_week', 1);
    }

    public function scopeNew($query)
    {
        return $query->where('is_new', 1);
    }

    public function scopeOfCategory($query, $categoryId)
    {
        return $query->where('category_id', $categoryId);
    }
}
