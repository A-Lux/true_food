<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'user_id',
        'email',
        'message',
        'attachment',
        'rating',
        'product_id'
    ];
}
