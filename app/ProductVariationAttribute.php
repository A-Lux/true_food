<?php

namespace App;

use App\Attribute;
use App\Dimention;
use App\ProductVariation;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ProductVariationAttribute extends Model
{
    use Filterable;
    use Translatable;

    protected $fillable = [
        'product_variation_id',
        'attribute_id',
        'value',
        'dimention_id',
        'is_main'
    ];
    protected $translatable = [



    ];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function dimention()
    {
        return $this->belongsTo(Dimention::class);
    }

    public function variation()
    {
        return $this->belongsTo(ProductVariation::class, 'product_variation_id');
    }
}
