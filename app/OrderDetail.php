<?php

namespace App;

use App\Product;
use App\GiftCard;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'entity_type',
        'entity_id',
        'order_id',
        'unit_quantity',
        'unit_cashback',
        'unit_price'
    ];

    protected $hidden = [
        'entity_type',
        'created_at',
        'updated_at'
    ];

    public function entity()
    {
        return $this->morphTo();
    }
}
