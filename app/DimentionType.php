<?php

namespace App;

use App\Dimention;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class DimentionType extends Model
{
    use Translatable;
    protected $translatable = [
        'name'
    ];
    protected $fillable = [
        'name'
    ];


    public function dimentions()
    {
        return $this->hasMany(Dimention::class);
    }
}
