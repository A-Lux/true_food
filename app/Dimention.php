<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Dimention extends Model
{
    use Translatable;
    protected $fillable = [
        'name',
        'slug',
        'short_name',
        'dimention_type_id'
    ];

    protected $translatable = ['name','short_name'];
}
