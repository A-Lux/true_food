<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class PaymentType extends Model
{
    use Translatable;
    protected $fillable = [
        'name'
    ];
    protected $translatable = [
        'name'
    ];
}
