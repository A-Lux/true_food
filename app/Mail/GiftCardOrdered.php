<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GiftCardOrdered extends Mailable
{
    use Queueable, SerializesModels;

    protected $subjectTitle;
    public $orderData;
    protected $viewName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $orderData, $view)
    {
        $this->subjectTitle = $subject;
        $this->orderData = $orderData;
        $this->viewName = $view;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function via()
    {
        return $this->view($this->viewName)
            ->subject($this->subjectTitle)
            ->from('truefood.kz@gmail.com');
    }
}
