<?php

namespace App\Providers;

use App\Events\GiftCardOrdered;
use App\Events\ProductsOrdered;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Listeners\SendGiftCardNotification;
use App\Listeners\ProductsOrderedNotification;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        GiftCardOrdered::class => [
            SendGiftCardNotification::class
        ],
        ProductsOrdered::class => [
            ProductsOrderedNotification::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
