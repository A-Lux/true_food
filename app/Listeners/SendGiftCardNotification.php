<?php

namespace App\Listeners;

use App\Events\GiftCardOrdered;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\GiftCardOrdered as GiftCardEmail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class SendGiftCardNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(GiftCardOrdered $event)
    {

        $admin = setting('kontakty.email');
        $user = User::where('email',$event->order->email)->first();
        try{
            Mail::send('emails.giftcard-ordered', ['orderData' => $event,'user' => $user], function ($m) use ($event) {
                $m->from('truefood.kz@gmail.com', 'truefood.kz');
                $m->to($event->order->email)->subject( 'Подарочные бонусы: ' . $event->orderDetail->unit_price);
            });

            Mail::send('emails.admin.giftcard-ordered', ['orderData' => $event, 'orderDetail'=> $event->orderDetail], function ($m) use ($event,$admin) {
                $m->from('truefood.kz@gmail.com', 'truefood.kz');

                $m->to($admin)->subject( 'Заказ подарочной карты на сумму ' . $event->orderDetail->unit_price);
            });
        }catch (\Exception $e){
            Log::error('Email error'. $e);
        }

    }
}
