<?php

namespace App\Paybox;

use Illuminate\Support\Facades\Http;

class Paybox
{
    private $params = [];
    private $secretKey;
    private $entry = 'https://api.paybox.money/payment.php';

    public function __construct($mode = null)
    {
        if ($mode == 'testing') {
            $this->pg_testing_mode = 1;
        }
        $this->secretKey = '6wfqi40siz3Pw4K8';
    }

    public function __set($name, $value)
    {
        $this->params[$name] = $value;
    }

    private function generateSignature()
    {
        ksort($this->params);
        array_unshift($this->params, 'payment.php');
        array_push($this->params, $this->secretKey);
        $this->pg_sig = md5(implode(';', $this->params));
        unset($this->params[0], $this->params[1]);
    }

    private function buildQuery()
    {
        return http_build_query($this->params);
    }

    public function getParams()
    {
        return $this->params;
    }

    public function pay()
    {
        $this->generateSignature();
        return $this->entry . '?' . $this->buildQuery();
    }

    public function setPgMerchantId($merchantId)
    {
        $this->params['pg_merchant_id'] = $merchantId;
    }

    public function setPgAmount($amount)
    {
        $this->params['pg_amount'] = $amount;
    }

    public function setPgSalt($salt)
    {
        $this->params['pg_salt'] = $salt;
    }

    public function setPgOrderId($orderId)
    {
        $this->params['pg_order_id'] = $orderId;
    }

    public function setPgDescription($description)
    {
        $this->params['pg_description'] = $description;
    }

    public function setPgCheckUrl($checkUrl)
    {
        $this->params['pg_check_url'] = $checkUrl;
    }

    public function setPgResultUrl($resultUrl)
    {
        $this->params['pg_result_url'] = $resultUrl;
    }

    public function setPgFailureUrl($failureUrl)
    {
        $this->params['pg_failure_url'] = $failureUrl;
    }

    public function setPgSuccessUrl($successUrl)
    {
        $this->params['pg_success_url'] = $successUrl;
    }
}

