<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $user_id;
    public $chat;
    public $message;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id,$chat,$message)
    {
        //
        $this->message  = $chat;
        $this->user_id = $user_id;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('User.Chats.'.$this->user_id);

    }
    public function broadcastAs()
    {
        return 'chat-created';
    }

    public function broadcastWith()
    {
        return [
          'chat' => $this->chat,
          'message' => $this->message,
        ];
    }
}
