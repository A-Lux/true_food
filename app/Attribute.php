<?php

namespace App;

use App\DimentionType;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'attribute_type_id',
        'dimention_type_id'
    ];

    public function dimentionType()
    {
        return $this->belongsTo(DimentionType::class);
    }
}
