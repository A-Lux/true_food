const mix = require("laravel-mix");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.options({ processCssUrls: false });

mix.js("resources/js/products.js", "public/js")
    .js("resources/js/app.js", "public/js")
    .js("resources/js/terms.js", "public/js")
    .js("resources/js/minor.js", "public/js")
    .js("resources/js/partials/productsCarousel.js", "public/js")
    .js("resources/js/home.js", "public/js")
    .js("resources/js/catalog.js", "public/js")
    .js("resources/js/cart.js", "public/js")
    .js("resources/js/card.js", "public/js")
    .js("resources/js/reviews.js", "public/js")
    .js("resources/js/cabinet.js", "public/js")
    .js("resources/js/giftcard.js", "public/js")
    .js("resources/js/contacts.js", "public/js")
    .sass("resources/sass/app.scss", "public/css")
    .sass("resources/sass/pages/home.scss", "public/css")
    .sass("resources/sass/pages/catalog.scss", "public/css")
    .sass("resources/sass/pages/card.scss", "public/css")
    .sass("resources/sass/pages/cart.scss", "public/css")
    .sass("resources/sass/pages/cabinet.scss", "public/css")
    .sass("resources/sass/pages/giftcard.scss", "public/css")
    .sass("resources/sass/pages/about.scss", "public/css")
    .sass("resources/sass/pages/contacts.scss", "public/css")
    .sass("resources/sass/pages/terms.scss", "public/css");
